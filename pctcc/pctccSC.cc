/* 
 * pctccSC.cc -- Standard MC for the PCTCC model on the simple cubic
 *               lattice
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

///////////////////////////////////////////////////////////////////////////////
//
// Include configuration and environments

#include <string>

#include "conf.hh"
#include "mcenv.hh"
#include <glsim/stochastic.hh>

///////////////////////////////////////////////////////////////////////////////
//
// Include the desired simulation classes

#include "mc.hh"


///////////////////////////////////////////////////////////////////////////////
//
// main

enum return_code
    {no_error=0,early_stop=1,usage_error=2,runtime_error=10,
     logic_error=20,other_exception=255} ;

int main(int argc,char *argv[])
{
  return_code rcode=no_error;

  try {

    MCEnvironment env;
    glsim::StochasticEnvironment senv;
    PCTCCConf<SCLattice> conf(env.Lx,env.Ly,env.Lz);
    PctccObs<SCLattice> obs(env,conf);

    glsim::prepare(argc,argv,env,conf);

    // if there are other scopes, must init them separately
    PctccSimulation<PCTCCConf<SCLattice> > sim(env,conf);
    obs.observe_first();
    sim.run();
    env.save();
    conf.save((const std::string&) env.configuration_file_fin);
  
  } catch (const glsim::Early_stop &e) {
    rcode=early_stop;
  } catch (const glsim::Usage_error &e) {
    rcode=usage_error;
  } catch (const glsim::Runtime_error& e) {
    std::cerr << e.what() << '\n';
    std::cerr << e.backtrace();
    rcode=::runtime_error;
  } catch (const glsim::Logic_error& e) {
    std::cerr << e.what() << '\n';
    std::cerr << e.backtrace();
    rcode=::logic_error;
  } catch (const std::exception &e) {
    std::cerr << "Exception: " << e.what() << '\n';
    rcode=::other_exception;
  }
  return rcode;
}
