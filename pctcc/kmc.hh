/* 
 * pctcc/kmc.hh -- Kinetic MC for the PCTCC model
 *
 * This contains the simulation class for single-spin-flip Metropolis
 * Monte Carlo of the lattice glass invented by Pica Ciamarra, Tarzia,
 * de Candia and Coniglio.
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _KMC_HH_
#define _KMC_HH_

#include <glsim/simulation.hh>

template <typename configuration>
class ListofMoves;

/*
 * Kinetic simulation declaration
 *
 */

template <typename Configuration>
class PctccKMC : public glsim::Simulation {
public:
  PctccKMC(KMCEnvironment&,Configuration&);

  const char *name() const;
  void step();
  void log_start_sim(),log_stop_sim();
  void log();

  KMCEnvironment &ienv;
  Configuration &iconf;

  bool check_config(bool full=false);

private:
  ListofMoves<Configuration> PossibleMoves;
  glsim::uniform_real ran;

  void count_parts();
  bool test_moves();
} ;

template <typename Configuration>
PctccKMC<Configuration>::PctccKMC(KMCEnvironment &env,Configuration &conf) :
  glsim::Simulation(env,conf),
  ienv(env),
  iconf(conf),
  PossibleMoves(ienv.GrandCanonical,conf)
{
  count_parts();
  PossibleMoves.build_move_list();
}

template <typename Configuration>
const char* PctccKMC<Configuration>::name() const
{
  return "Kinetic MC of the Pica Ciamarra - Tarzia - de Candia - Coniglio model";
}

template <typename Configuration>
void PctccKMC<Configuration>::count_parts()
{
  ienv.nparts=0;
  for (typename Configuration::Graph_t::node_literator s=iconf.lat.begin();
       s!=iconf.lat.end(); ++s)
    if (*s!=Configuration::Graph_t::nilnode) ienv.nparts++;
}

template <typename Configuration>
std::ostream& operator<<(std::ostream& o,ListofMoves<Configuration>& Pm);

/*******************************************************************************
 *
 * List of allowed moves
 *
 */

template <typename configuration>
class ListofMoves {
private:
  bool           GrandCanonical;
  configuration& iconf;
  typedef typename configuration::Graph_t::node_t node_t;
  
  struct empty_nonempty_neighbs : public UnaryFunction<node_t>  {
    int      point_here;
    nodeid_t here;
    node_t*  neighb_points_here;
    std::vector<node_t*> empty;
    std::vector<node_t*> nonempty;
    
    empty_nonempty_neighbs(nodeid_t here_) : point_here(0), here(here_) {}
    
    void operator()(node_t& n) {
      if (n==configuration::Graph_t::nilnode) empty.push_back(&n);
      else {
	nonempty.push_back(&n);
	if (n==here) {
	  point_here++;
	  neighb_points_here=&n;
	}
      }
    }
  } ;

public:
  struct movedesc_trans {
    node_t   *from,*to;
    nodeid_t newid;

    movedesc_trans(node_t *f,node_t *t,nodeid_t n) :
      from(f), to(t), newid(n) {}

  } ;
  struct movedesc_create {
    node_t   *dest;
    nodeid_t id;

    movedesc_create(node_t *d,nodeid_t n) :
      dest(d), id(n) {}
    
  } ;
  typedef std::vector<movedesc_trans> translist_t;
  typedef std::vector<movedesc_create> createlist_t;
  typedef std::vector<node_t*> occlist_t;
  translist_t moves_trans;
  createlist_t moves_create;
  occlist_t occ_sites;

  ListofMoves(bool GrandCan,configuration& c) : GrandCanonical(GrandCan), iconf(c) {}
  int  Ntrans() const {return moves_trans.size();}
  int  Ncreate() const {return moves_create.size();}
  int  Ndestroy() const {return occ_sites.size();}
  void clear_moves();
  void build_move_list();
  void do_move_trans(int),do_move_create(int),do_move_destroy(int);

protected:
  void add_moves_with_dest(const typename configuration::Graph_t::node_iterator&);
  void remove_moves_with_dest(node_t*);
  void add_trans_move(node_t*,node_t*,node_t*);
  void add_create_move(node_t*,node_t*);

  friend std::ostream& operator<< <>(std::ostream&,ListofMoves&);
} ;

template <typename configuration>
void ListofMoves<configuration>::clear_moves()
{
  moves_trans.clear();
  moves_create.clear();
  occ_sites.clear();
}

template <typename configuration>
void ListofMoves<configuration>::build_move_list()
{
  clear_moves();
  typename configuration::Graph_t::node_iterator site(iconf.lat);

  for (site.moveto(0,0,0); site!=iconf.lat.end(); ++site) {
    if (*site==configuration::Graph_t::nilnode)
      add_moves_with_dest(site);
    else if (GrandCanonical)
      occ_sites.push_back(&(*site));
  }
}

template <typename configuration>
void ListofMoves<configuration>::
add_moves_with_dest(const typename configuration::Graph_t::node_iterator &site)
{
  empty_nonempty_neighbs ene(site.node_id());
  for_each_neighbour(site,ene);
  // if more than one neighbour points here, there are no possible moves to here 
  // (excluding, as we do, pure rotations)
  if (ene.point_here>1) return;
  if (ene.point_here==1) {    // only site pointing here can move here
   // add move: jump here pointing to old site
    add_trans_move(ene.neighb_points_here,&(*site),ene.neighb_points_here);
    for (typename std::vector<node_t*>::iterator m=ene.empty.begin();
	 m!=ene.empty.end(); ++m)
      add_trans_move(ene.neighb_points_here,&(*site),*m);  // add move: jump here pointing to other free neighbour
    return;
  }
  // No one points here, all nonempty neighbours can jump in
  // and a new particle can be placed
  for (typename std::vector<node_t*>::iterator n=ene.nonempty.begin();
       n!=ene.nonempty.end(); ++n) {
    // we skip next check because there are no pure rotations
    // if (*n!=site.node_id()) add_move(n,n,site);   // add move point to here
    add_trans_move(*n,&(*site),*n);      // add move: jump here pointing to old site
    for (typename std::vector<node_t*>::iterator m=ene.empty.begin();
	 m!=ene.empty.end(); ++m) {
      add_trans_move(*n,&(*site),*m);  // add move: jump here pointing to other free neighbour
    }
  }
  if (!GrandCanonical) return;
  for (typename std::vector<node_t*>::iterator m=ene.empty.begin();
       m!=ene.empty.end(); ++m)
    add_create_move(&(*site),*m);    // add move: create here pointing to free neighbour
}

template <typename configuration>
void ListofMoves<configuration>::add_trans_move(node_t* from,node_t* to,node_t* point)
{
  nodeid_t p=iconf.lat.nodeid(point);
  moves_trans.push_back(movedesc_trans(from,to,p));
}

template <typename configuration>
void ListofMoves<configuration>::add_create_move(node_t* dest,node_t* point)
{
  nodeid_t p=iconf.lat.nodeid(point);
  moves_create.push_back(movedesc_create(dest,p));
}

template <typename configuration>
void ListofMoves<configuration>::remove_moves_with_dest(node_t *dest)
{
  typename translist_t::iterator st;
  st=moves_trans.begin();
  while (st!=moves_trans.end()) {
    if (st->to==dest) st=moves_trans.erase(st);
    else ++st;
  }
  typename createlist_t::iterator sc;
  sc=moves_create.begin();
  while (sc!=moves_create.end()) {
    if (sc->dest==dest) sc=moves_create.erase(sc);
    else ++sc;
  }
}

template <typename configuration>
void ListofMoves<configuration>::do_move_trans(int m)
{
  // perform move
  node_t* from=moves_trans[m].from;
  node_t* to=moves_trans[m].to;
  nodeid_t newid=moves_trans[m].newid;
  nodeid_t oldid=*from;
  *from=configuration::Graph_t::nilnode;
  *to=newid;

  // remove moves that have become invalid:
  // - moves to site just occupied
  // - moves to site pointed by site just occupied
  // - moves that point to site just occupied
  // - moves originating in site just vacated
  // - creation of particles in site just occupied
  // - creation of particles in site pointed to (handled below when
  //   deleting moves prior to adding moves to free neighbours)
  // - creation of particles pointing to site just occupied
  // - destruction of particle in site just vacated
  typename translist_t::iterator st;
  st=moves_trans.begin();
  while (st!=moves_trans.end()) {
    if (st->to==to ||
	st->to==&(iconf.lat.node_ref(newid)) ||
	st->newid==iconf.lat.nodeid(to) ||
	st->from==from) st=moves_trans.erase(st);
    else ++st;
  }
  typename createlist_t::iterator sc;
  sc=moves_create.begin();
  while (sc!=moves_create.end()) {
    if (sc->dest==to ||
	sc->id==iconf.lat.nodeid(to) ) sc=moves_create.erase(sc);
    else ++sc;
  }
  typename occlist_t::iterator so;
  so=occ_sites.begin();
  while (so!=occ_sites.end()) {
    if (*so==from) so=occ_sites.erase(so);
    else ++so;
  }

  // add new possible moves:
  // - moves to free neighbours of new site (includes site just vacated)
  // - moves to free neighbours of original site (some are already in list but new
  //   ones may be possible)
  // - creation of particles in the two cases above
  // - destruction of particle in site just occupied
  typename configuration::Graph_t::node_iterator site(iconf.lat);
  typename std::vector<node_t*> psites;
  site.moveto(from);
  empty_nonempty_neighbs ene(site.node_id());
  for_each_neighbour(site,ene);
  psites.insert(psites.end(),ene.empty.begin(),ene.empty.end());
  site.moveto(to);
  for_each_neighbour(site,ene);
  psites.insert(psites.end(),ene.empty.begin(),ene.empty.end());
  for (typename std::vector<node_t*>::iterator n=psites.begin();
       n!=psites.end(); ++n) {
    remove_moves_with_dest(*n);
    site.moveto(*n);
    add_moves_with_dest(site);
  }
  if (GrandCanonical) occ_sites.push_back(to);
}

template <typename configuration>
void ListofMoves<configuration>::do_move_create(int m)
{
  // perform move
  node_t* dest=moves_create[m].dest;
  nodeid_t id=moves_create[m].id;
  *dest=id;

  // remove translations that have become invalid:
  // - moves to site just occupied
  // - moves to site pointed by site just occupied
  // - moves that point to site just occupied
  typename translist_t::iterator st;
  st=moves_trans.begin();
  while (st!=moves_trans.end()) {
    if (st->to==dest ||
	st->to==&(iconf.lat.node_ref(id)) ||
	st->newid==iconf.lat.nodeid(dest)) st=moves_trans.erase(st);
    else ++st;
  }

  // remove creations that have become invalid:
  // - to site just occupied
  // - to site pointed by site just occupied
  // - pointeng to site just occupied
  typename createlist_t::iterator sc;
  sc=moves_create.begin();
  while (sc!=moves_create.end()) {
    if (sc->dest==dest ||
	sc->dest==&(iconf.lat.node_ref(id)) ||
	sc->id==iconf.lat.nodeid(dest)) sc=moves_create.erase(sc);
    else ++sc;
  }

  // add new possible moves:
  // - moves to free neighbours of new site
  // - destruction of new site
  typename configuration::Graph_t::node_iterator site(iconf.lat);
  typename std::vector<node_t*> psites;
  site.moveto(dest);
  empty_nonempty_neighbs ene(site.node_id());
  for_each_neighbour(site,ene);
  psites.insert(psites.end(),ene.empty.begin(),ene.empty.end());
  for (typename std::vector<node_t*>::iterator n=psites.begin();
       n!=psites.end(); ++n) {
    remove_moves_with_dest(*n);
    site.moveto(*n);
    add_moves_with_dest(site);
  }
  occ_sites.push_back(dest);

}

template <typename configuration>
void ListofMoves<configuration>::do_move_destroy(int m)
{
  // perform move
  node_t* dest=occ_sites[m];
  *dest=configuration::Graph_t::nilnode;

  // remove translations that have become invalid:
  // - moves from site just destroyed
  typename translist_t::iterator st;
  st=moves_trans.begin();
  while (st!=moves_trans.end()) {
    if (st->from==dest) st=moves_trans.erase(st);
    else ++st;
  }
  // remove destruction that has become invalid
  // - site just destroyed
  typename occlist_t::iterator so;
  so=occ_sites.begin();
  so+=m;
  occ_sites.erase(so);
  
  // add new possible moves:
  // - moves site just destroyed
  // - moves to free neighbours site just destroyed(some are already in list but new
  //   ones may be possible)
  // - creation of particles in the two cases above
  typename configuration::Graph_t::node_iterator site(iconf.lat);
  typename std::vector<node_t*> psites;
  site.moveto(dest);
  add_moves_with_dest(site);
  empty_nonempty_neighbs ene(site.node_id());
  for_each_neighbour(site,ene);
  psites.insert(psites.end(),ene.empty.begin(),ene.empty.end());
  for (typename std::vector<node_t*>::iterator n=psites.begin();
       n!=psites.end(); ++n) {
    remove_moves_with_dest(*n);
    site.moveto(*n);
    add_moves_with_dest(site);
  }
}


//
// Next two functions are just for debugging
//

template <typename configuration>
std::ostream& operator<<(std::ostream& o,ListofMoves<configuration>& Pm)
{
  for (int m=0; m<Pm.nMoves(); m++)
    o << "Move "<<  m << ": from " << Pm.iconf.lat.nodeid(Pm.moves[m].from) <<
      " to " << Pm.iconf.lat.nodeid(Pm.moves[m].to) << " id " <<
      Pm.moves[m].newid << '\n';
  return o;
}

template <typename Configuration>
bool PctccKMC<Configuration>::test_moves()
{
  if (!check_config()) {
    std::cerr << "Error, in test_moves(): starting configuration invalid\n";
    exit(2);
  }

  bool pass=true;
  for (int m=0; m<PossibleMoves.Ntrans(); m++) {
    bool pass_local=true;
    nodeid_t old=*(PossibleMoves.moves_trans[m].from);
    *(PossibleMoves.moves_trans[m].from)=Configuration::Graph_t::nilnode;
    if (*(PossibleMoves.moves_trans[m].to)!=Configuration::Graph_t::nilnode)
      {std::cerr << "BAD MOVE pointing to nonvoid\n"; exit(1);}
    *(PossibleMoves.moves_trans[m].to)=PossibleMoves.moves_trans[m].newid;
    if (!check_config()) pass_local=false;;
    *(PossibleMoves.moves_trans[m].from)=old;
    *(PossibleMoves.moves_trans[m].to)=Configuration::Graph_t::nilnode;
    if (!check_config()) pass_local=false;;
    if (!pass_local) {
      std::cerr << "BAD Move: from " << iconf.lat.nodeid(PossibleMoves.moves_trans[m].from) <<
	" to " << iconf.lat.nodeid(PossibleMoves.moves_trans[m].to) << " id " <<
	PossibleMoves.moves_trans[m].newid;
      int i,j,k;
      iconf.lat.posl(PossibleMoves.moves_trans[m].newid,i,j,k);
      std::cerr << " (" << i << ',' << j << ',' << k << ")\n";
    }
    pass&=pass_local;
  }
  for (int m=0; m<PossibleMoves.Ncreate(); m++) {
    bool pass_local=true;
    if (*(PossibleMoves.moves_create[m].dest)!=Configuration::Graph_t::nilnode)
      {std::cerr << "BAD MOVE creating at nonvoid\n"; exit(1);}
    *(PossibleMoves.moves_create[m].dest)=PossibleMoves.moves_create[m].id;
    if (!check_config()) pass_local=false;;
    *(PossibleMoves.moves_create[m].dest)=Configuration::Graph_t::nilnode;
    if (!check_config()) pass_local=false;;
    if (!pass_local) {
      std::cerr << "BAD Move: creating at " <<
	iconf.lat.nodeid(PossibleMoves.moves_create[m].dest) << " with id " <<
	PossibleMoves.moves_create[m].id;
      int i,j,k;
      iconf.lat.posl(PossibleMoves.moves_create[m].id,i,j,k);
      std::cerr << " (" << i << ',' << j << ',' << k << ")\n";
    }
    pass&=pass_local;
  }
  for (int m=0; m<PossibleMoves.Ndestroy(); m++) {
    bool pass_local=true;
    if (*(PossibleMoves.occ_sites[m])==Configuration::Graph_t::nilnode)
      {pass_local=false; std::cerr << "BAD MOVE destroying void\n"; exit(1);}
    pass&=pass_local;
  }
  return pass;
}

/******************************************************************************
 *
 * Kinetic MC step (grand canonical)
 *
 * WARNING: This function is only good for graphs with constant number of neighbours
 * and alpha>0
 *
 */
template <typename Configuration>
void PctccKMC<Configuration>::step()
{
  int    N=iconf.vol;
  int    Nc=iconf.lat.coordination_number();

  ienv.last_alpha=ienv.alpha;
  ienv.alpha= ienv.alphar== 0 ? ienv.alpha0 : ienv.alpha0*exp(ienv.alphar*iconf.time);
  double Wtrans=(double) 1/(2*N*Nc*Nc);
  double Wcreate=(double) 1/(2*N*Nc);
  double Wdestroy=exp(-ienv.alpha)/(2*N*Nc);
  double P1=PossibleMoves.Ntrans()*Wtrans;
  double P2=P1 + (double) PossibleMoves.Ncreate()*Wcreate;
  double Ptot=P2 + (double) PossibleMoves.Ndestroy()*Wdestroy;

#ifdef DEBUG
  std::cerr << "(DD) Alpha= " << ienv.alpha << ", exp(-alpha)= " << exp(-ienv.alpha) << '\n';
  std::cerr << "(DD) Wtrans= " << Wtrans << ", Wcreate= " << Wcreate << ", Wdestroy= " <<
    Wdestroy << '\n';
  std::cerr << "(DD) Moves found: trans " << PossibleMoves.Ntrans() <<
    " create " << PossibleMoves.Ncreate() << " destroy " << PossibleMoves.Ndestroy() <<
    " (with " << nparts << " particles)\n";
  std::cerr << "(DD) P1= " << P1 << ", P2= " << P2 << ", Ptot= " << Ptot << '\n';
  if (PossibleMoves.Ndestroy()!=nparts) exit(2);
  if (!test_moves()) {
    std::cerr << "(DD) Move test failed\n";
    exit(2);
  }
  int nt=PossibleMoves.Ntrans();
  int nc=PossibleMoves.Ncreate();
  int nd=PossibleMoves.Ndestroy();
  PossibleMoves.build_move_list();
  if (nt!=PossibleMoves.Ntrans() || nc!=PossibleMoves.Ncreate() ||
      nd!=PossibleMoves.Ndestroy() ) {
    std::cerr << "(DD) Move test failed: nmoves " << nt << ' ' << nc << ' ' << nd << '\n';
    std::cerr << "     da capo         : nmoves " << PossibleMoves.Ntrans() << ' ' <<
      PossibleMoves.Ncreate() << ' ' << PossibleMoves.Ndestroy() << '\n';
    exit(2);
  }
  std::cerr << "(DD) Moves tested\n";
#endif

  if (Ptot==0) {
    std::cout << "System jammed\n";
    raise(2);
    return;
  }
  
  double r=Ptot*ran();
  int   m,move;
  ienv.last_nparts=ienv.nparts;
  if (r<P1) {
    move=0;
    m=floor(r/Wtrans);
    PossibleMoves.do_move_trans(m);
  } else if (r<P2) {
    move=1;
    m=floor((r-P1)/Wcreate);
    PossibleMoves.do_move_create(m);
    ++ienv.nparts;
  } else {
    move=2;
    m=floor((r-P2)/Wdestroy);
    PossibleMoves.do_move_destroy(m);
    --ienv.nparts;
  }
  iconf.time+=1./(N*Ptot);  // 1/N is the time unit
  ienv.time_completed=iconf.time;

#ifdef DEBUG
  switch (move) {
  case 0:
    std::cerr << "(DD) --- r= " << r << ", applied trans move " << m << '\n';
    break;
  case 1:
    std::cerr << "(DD) --- r= " << r << ", applied create move " << m << '\n';
    break;
  case 2:
    std::cerr << "(DD) --- r= " << r << ", applied destroy move " << m << '\n';
    break;
  }
#endif

  // Finalizacion (engañando a run())
  ienv.run_completed=iconf.time>=ienv.time_requested;
}

/*
 * Logging functions
 *
 */

template <typename Configuration>
void PctccKMC<Configuration>::log_start_sim()
{
  glsim::logs(glsim::info)  << "Starting time = " << iconf.time << '\n';
  if (check_config())
    glsim::logs(glsim::info) << "Configuration at time " << iconf.time << " checked OK\n";
  else exit(1);
  glsim::logs(glsim::info) << "     Steps       Time Nparts Density Ntrans Ncreat  Ndest\n";
  log();
}

template <typename Configuration>
void PctccKMC<Configuration>::log()
{
  char buf[300];
  sprintf(buf,"%10d %10.4e %6d %7.4f %6d %6d %6d\n",ienv.steps_completed,iconf.time,
	 ienv.nparts,(double)ienv.nparts/iconf.vol,PossibleMoves.Ntrans(),PossibleMoves.Ncreate(),
	 PossibleMoves.Ndestroy());
  glsim::logs(glsim::info) << buf;
}

template <typename Configuration>
bool PctccKMC<Configuration>::check_config(bool full_check)
{
  bool ok=true;

  if (full_check) {
    for (typename Configuration::Graph_t::node_iterator s(iconf.lat); s!=iconf.lat.end(); s++) {
      if (*s==Configuration::Graph_t::nilnode) continue;
      typename Configuration::Graph_t::node_t *id=&(iconf.lat.node_ref(*s));
      bool isneigh=false;
      for (int i=0; i<s.nNeighbours(); i++)
	if (&(s.neighbour(i))==id) {isneigh=true; break;}
      if (!isneigh)
	std::cout << "ERROR: node " << s.node_id() << " points to nonneighbour " << *s << '\n';
    }
  }

  for (typename Configuration::Graph_t::node_iterator s(iconf.lat); s!=iconf.lat.end(); s++) {
    if (*s==Configuration::Graph_t::nilnode) continue;
    if (iconf.lat.node_value(*s)!=Configuration::Graph_t::nilnode) {
      ok=false;
      std::cout << "ERROR: node " << s.node_id() << " points to nonempty node " << *s << '\n';
    }
  }
  return ok;
}

template <typename Configuration>
void PctccKMC<Configuration>::log_stop_sim()
{
  //  test_moves();
  if (check_config())
    glsim::logs(glsim::info) << "Configuration at time " << iconf.time << " checked OK\n";
  else
    glsim::logs(glsim::info) << "ERROR: Bad configuration at time " << iconf.time << "\n";
}


///////////////////////////////////////////////////////////////////////////////
//
// Observable

#include <glsim/observable.hh>

class PctccObsParameters : public glsim::Parameters {
public:
  PctccObsParameters(const char* scope=Parameters::default_scope);
} ;

PctccObsParameters::PctccObsParameters(const char* scope) :
  Parameters(scope)
{
  parm_file_options().add_options()
    ("pctcc.obs_file_prefix",po::value<std::string>()->default_value("obs"),"observation file prefix")
    ("pctcc.obs_interval",po::value<double>()->default_value(0))
    ;
}

template <typename Latt>
class PctccObs : public glsim::KMCObservable {
public:
  PctccObs(KMCEnvironment&,PCTCCConf<Latt>&);

  void interval_and_file();
  void write_header(),observe();

private:
  PctccObsParameters par;
  KMCEnvironment      &env;
  PCTCCConf<Latt>    &conf;
} ;

template <typename Latt>
PctccObs<Latt>::PctccObs(KMCEnvironment& e,PCTCCConf<Latt>& c) :
  KMCObservable(e),
  par(e.scope()),
  env(e),
  conf(c)
{}

template <typename Latt>
void PctccObs<Latt>::interval_and_file()
{
  obs_file_prefix=par.value("pctcc.obs_file_prefix").as<std::string>();
  obs_interval=par.value("pctcc.obs_interval").as<double>();
}

template <typename Latt>
void PctccObs<Latt>::write_header()
{
  fprintf(of,"# Pctcc standard observer, interval: %g\n",obs_interval);
  fprintf(of,"#       Time     T/mu Nparts Density   SpVol\n");
}

template <typename Latt>
void PctccObs<Latt>::observe()
{
  double dens=(double) env.last_nparts/conf.vol;
  double spvol= env.last_nparts== 0 ? 0 : 1./dens;

  fprintf(of,"%12.8g %8.4f %6d %7.4f %7.4f\n",obs_time,1./env.last_alpha,env.last_nparts,dens,spvol);
}


#endif /* _KMC_HH_ */
