/* 
 * sgsus.hh -- Spin glass susceptibility for PCTCC
 *
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _SGSUS_HH_
#define _SGSUS_HH_

// Parameters

class SGsusSC_parameters : public SGsusSC_parameters_base {
public:
  SGsusSC_parameters();
};

SGsusSC_parameters::SGsusSC_parameters() :
  SGsusSC_parameters_base()
{
  ctrl_file_options.add_options()
    ("SGsusSC.file_prefix",po::value<std::string>(),"SGsuscept file name prefix")
    ("SGsusSC.step",po::value<int>(),"SG suscept write step")
    ;
}

// Environment

class SGsusSC_environment : public SGsusSC_environment_base {
public:
  std::string sgsus_fname;
  int         sgsus_step;

  SGsusSC_environment(int argc,char *argv[],SGsusSC_parameters &);
};

SGsusSC_environment::SGsusSC_environment(int argc,char *argv[],
					     SGsusSC_parameters& par) :
  SGsusSC_environment_base(argc,argv,par)
{
  sgsus_fname=par.value("SGsusSC.file_prefix").as<std::string>()+fin_infix+".dat";
  sgsus_step=par.value("SGsusSC.step").as<int>();
}

// Suscept

template <typename environment>
class SGsusSC : public Observable {
private:
  typedef PctccSimulation<SC_conf,environment> Sim;

  Sim         &sim;
  SC_conf     &iconf;
  environment &ienv;
  spindirSC   conf;
  FILE        *of;

  double      kmin,xifac;

  void start(Simulation::run_mode);
  void do_observation();
  int pdiff(int i, int j, int L);

public:
  SGsusSC(Simulation &sim);
  ~SGsusSC() {fclose(of);}
} ;

template <typename environment>
SGsusSC<environment>::SGsusSC(Simulation &s) :
  Observable("SGsus observable",
   	     dynamic_cast<Sim&>(s).ienv,
   	     dynamic_cast<Sim&>(s).iconf,
   	     dynamic_cast<Sim&>(s).ienv.sgsus_step),
  sim(dynamic_cast<Sim&>(s)),
  iconf(sim.iconf),
  ienv(sim.ienv),
  conf(sim.iconf.lat)
{
  kmin=2*M_PI/iconf.lat.Xsize();
  xifac=2*sin(kmin/2.);
  xifac=1./(xifac*xifac);
}

template <typename environment>
void SGsusSC<environment>::start(Simulation::run_mode rmode)
{
  switch (rmode) {
  case Simulation::new_run:
    of=fopen(ienv.sgsus_fname.c_str(),"w");
    assert(of);
    fprintf(of,"# SG suceptibility, step: %d\n",ienv.sgsus_step);
    fprintf(of,"# t_w  C(k=0)   C(kmin).real   C(kmin).imag    xi      Csg(k=0)   Csg(kmin).real   Csg(kmin).imag    xisg\n");
    break;
  case Simulation::continuation_run:
    of=fopen(ienv.sgsus_fname.c_str(),"a");
    assert(of);
    break;
  }
  Observable::start(rmode);
}

template <typename environment>
void SGsusSC<environment>::do_observation()
{
  conf.update();
  spindirSC::SpinLattice_t::node_iterator c(conf.spinlat);

  int si[3],sj[3];
  double C0=0,Ckmin_r=0,Ckmin_i=0;
  double Csg0=0,Csgkmin_r=0,Csgkmin_i=0;
  for (int ix=0; ix<conf.spinlat.Xsize(); ix++) 
    for (int jx=0; jx<conf.spinlat.Xsize(); jx++) {

      int j=pdiff(ix,jx,conf.spinlat.Xsize());
      for (int iy=0; iy<conf.spinlat.Ysize(); iy++)
	for (int jy=0; jy<conf.spinlat.Ysize(); jy++) {

	  int k=pdiff(iy,jy,conf.spinlat.Ysize());
	  for (int iz=0; iz<conf.spinlat.Zsize(); iz++)
	    for (int jz=0; jz<conf.spinlat.Zsize(); jz++) {

	      int l=pdiff(iz,jz,conf.spinlat.Zsize());

	      si[0]=conf.spin_component(conf.spinlat(ix,iy,iz),0);
	      si[1]=conf.spin_component(conf.spinlat(ix,iy,iz),1);
	      si[2]=conf.spin_component(conf.spinlat(ix,iy,iz),2);
	      sj[0]=conf.spin_component(conf.spinlat(jx,jy,jz),0);
	      sj[1]=conf.spin_component(conf.spinlat(jx,jy,jz),1);
	      sj[2]=conf.spin_component(conf.spinlat(jx,jy,jz),2);

	      int S0=si[0]*sj[0];
	      int S1=si[1]*sj[1];
	      int S2=si[2]*sj[2];
	      int SS=S0+S1+S2;

	      C0+=SS;
	      Ckmin_r+=SS*( cos(kmin*j) + cos(kmin*k) * cos(kmin*l) )/3.;  // promedio las 3 direcciones de k
	      Ckmin_i+=SS*sin(kmin*j);

	      SS=S0*S0 + S1*S1 + S2*S2;
	      Csg0+=SS;
	      Csgkmin_r+=SS*( cos(kmin*j) + cos(kmin*k) * cos(kmin*l) )/3.;  // promedio las 3 direcciones de k
	      Csgkmin_i+=SS*sin(kmin*j);

	    }
	}
    }
  C0/=iconf.vol;
  Ckmin_r/=iconf.vol;
  Ckmin_i/=iconf.vol;
  Csg0/=iconf.vol;
  Csgkmin_r/=iconf.vol;
  Csgkmin_i/=iconf.vol;

  double xi=sqrt(xifac*(C0/Ckmin_r-1));
  double xisg=sqrt(xifac*(Csg0/Csgkmin_r-1));
  fprintf(of,"%g  %g  %g  %g  %g     %g %g %g %g\n",iconf.time,
	  C0,Ckmin_r,Ckmin_i,xi,
	  Csg0,Csgkmin_r,Csgkmin_i,xisg);
  fflush(of);
}

template <typename environment>
inline int SGsusSC<environment>::pdiff(int i, int j, int L)
{
  return i-j-L*rint((double)(i-j)/L);
}

#endif /* _SGSUS_HH_ */
