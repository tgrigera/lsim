/* 
 * pctcc-crystal3.cc -- Generate a PCTCC crystal on the SC lattice
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#include "glsim/errdeb.h"
#include "../graph/lattice3D.hh"
#include "pctcc.hh"

typedef PeriodicSCLattice<nodeid_t> SCLattice;

class SCParam : public PctccConfiguration_constructParam {
private:
  int Lx_,Ly_,Lz_;
public:
  SCParam() {}
  SCParam& Lx(int L) {Lx_=L; return *this;}
  SCParam& Ly(int L) {Ly_=L; return *this;}
  SCParam& Lz(int L) {Lz_=L; return *this;}

  friend class PctccConfiguration<SCLattice>;
} ;

template<>
PctccConfiguration<SCLattice>::
PctccConfiguration(const PctccConfiguration_constructParam &par) :
  Configuration("PCTCC on the SC lattice"),
  lat(dynamic_cast<const SCParam&>(par).Lx_,
      dynamic_cast<const SCParam&>(par).Ly_,
      dynamic_cast<const SCParam&>(par).Lz_)
{
  vol=lat.nNodes();
  deflt(0);
}

typedef PctccConfiguration<SCLattice> SC_conf;


void cryst(SC_conf& conf)
{
  for (int z=0; z<conf.lat.Zsize(); ++z) {
    for (int y=0; y<conf.lat.Ysize(); ++y) {
      for (int x=0; x<conf.lat.Xsize(); ++x) {
  	int a = (x + 2*y + 3*z) % 7;
	switch (a) {
	case 0:
	  conf.lat(x,y,z)=SCLattice::nilnode;
	  break;
	case 1:
	  conf.lat(x,y,z)=conf.lat.node_id_protected(x-1,y,z);
	  break;
	case 2:
	  conf.lat(x,y,z)=conf.lat.node_id_protected(x,y-1,z);
	  break;
	case 3:
	  conf.lat(x,y,z)=conf.lat.node_id_protected(x,y,z-1);
	  break;
	case 4:
	  conf.lat(x,y,z)=conf.lat.node_id_protected(x,y,z+1);
	  break;
	case 5:
	  conf.lat(x,y,z)=conf.lat.node_id_protected(x,y+1,z);
	  break;
	case 6:
	  conf.lat(x,y,z)=conf.lat.node_id_protected(x+1,y,z);
	  break;
	}
	int ix,iy,iz;
	conf.lat.posl(conf.lat(x,y,z),ix,iy,iz);
      }
    }
  }
}

random_number_generator RNG(gsl_rng_mt19937,0);

void check_config(SC_conf &conf)
{
  bool ok=true;

  for (SCLattice::node_iterator s(conf.lat); s!=conf.lat.end(); s++) {
    if (*s==SCLattice::nilnode) continue;
    if (conf.lat.node_value(*s)!=SCLattice::nilnode) {
      ok=false;
      std::cout << "ERROR: node " << s.node_id() << " points to nonempty node " << *s << '\n';
    }
  }
  if (ok) std::cout << "Configuration " << conf.time << " checked OK\n";
}

int main(int argc,char *argv[])
{
  if (argc!=4) {
    std::cout << "usage: " << argv[0] << " L Ndefects fname(output)\n";
    exit(1);
  }

  int L=atoi(argv[1]);
  int ndef=atoi(argv[2]);
  char *fname=argv[3];

  SC_conf conf(SCParam().Lx(L).Ly(L).Lz(L));
  cryst(conf);
  if (ndef>0) conf.lat(0,0,1)=SCLattice::nilnode;
  check_config(conf);
  conf.save(fname);
  conf.load(fname);
  check_config(conf);
}

