/* 
 * crystalmass.hh -- 
 *
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */


#ifndef _CRYSTALMASS_H_
#define _CRYSTALMASS_H_

// Parameters

class crystalmass_parameters : public crystalmass_parameters_base {
public:
  crystalmass_parameters();
};

crystalmass_parameters::crystalmass_parameters() :
  crystalmass_parameters_base()
{
  ctrl_file_options.add_options()
    ("crystalmass.step",po::value<int>(),"write step")
    ("crystalmass.file_prefix",po::value<std::string>(),"")
    ;
}

// Environment

class crystalmass_environment : public crystalmass_environment_base {
public:
  std::string crystal_fname;
  int         CM_step;

  crystalmass_environment(int argc,char *argv[],crystalmass_parameters &);
};

crystalmass_environment::crystalmass_environment(int argc,char *argv[],
				   crystalmass_parameters& par) :
  crystalmass_environment_base(argc,argv,par)
{
  crystal_fname=par.value("crystalmass.file_prefix").as<std::string>()+fin_infix+".dat";
  CM_step=par.value("crystalmass.step").as<int>();
}

// 

template <typename environment>
class crystalmass : public Observable {
private:
  typedef PctccSimulation<SC_conf,environment> Sim;

  Sim         &sim;
  SC_conf     &iconf;
  environment &ienv;

  FILE        *of;

  void start(Simulation::run_mode);
  void do_observation();

public:
  crystalmass(Simulation &sim);
  ~crystalmass();
} ;

template <typename environment>
crystalmass<environment>::crystalmass(Simulation &s) :
  Observable("Observable for crystal mass",
   	     dynamic_cast<Sim&>(s).ienv,
   	     dynamic_cast<Sim&>(s).iconf,
   	     dynamic_cast<Sim&>(s).ienv.CM_step),
  sim(dynamic_cast<Sim&>(s)),
  iconf(sim.iconf),
  ienv(sim.ienv)
{
}

template <typename environment>
void crystalmass<environment>::start(Simulation::run_mode rmode)
{
  switch (rmode) {
  case Simulation::new_run:
    of=fopen(ienv.crystal_fname.c_str(),"w");
    fprintf(of,"# time   ncrystals/Nsite\n");
    break;
  case Simulation::continuation_run:
    of=fopen(ienv.crystal_fname.c_str(),"a");
    break;
  }
  Observable::start(rmode);
}

template <typename environment>
crystalmass<environment>::~crystalmass()
{
  fclose(of);
}

template <typename environment>
void crystalmass<environment>::do_observation()
{
  SCLattice& lat=iconf.lat;
  SCLattice::node_iterator site(lat);
  int counter; int n;
  int crystal_sites;

  // en iconf tengo la configuracions
  crystal_sites=0;
  for (site.moveto(0,0,0); site!=iconf.lat.end(); ++site) // Recorro la red desde el (0,0,0) hasta el ultimo.
    { 
      if (*site==SCLattice::nilnode) // saber si site esta vacio    
	{
	  counter=0;
	  for(n=0; n<6; n++) {// Analizo los 6 vecinos
	    if(site.neighbour(n)!=site.node_id()) break;
	    counter++;
	    // saber si los vecinos apuntan a site site.neighbour(n)
	    // con n=0,5 me dice el numero de vecino. La idea es
	    // chequear si los 6 vecinos apuntan. Por cada uno que
	    // apunte, incremento counter.
	  }
	  if(counter==6) crystal_sites++; // Un cristal es un sitio vacio rodeado de 6 sitios ocupados que apuntan a el.	
	}
    }	//for de sitios

  fprintf(of,"%g %g\n",iconf.time,1.*crystal_sites/iconf.vol); // Imprime cantidad de cristales normalizando por cantidad de sitios vs tiempo. Se podria normalizar usando densidad (mejor). 
  fflush(of);

} //cierra void
	 
#endif /* _CRYSTALMASS_H_ */

