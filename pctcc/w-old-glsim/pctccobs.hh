/* 
 * pctccobs.hh -- Several observables for  pctcc configurations
 *
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _PCTCCOBS_HH_
#define _PCTCCOBS_HH_

#include "../graph/lattice3D.hh"
#include "pctcc.hh"

typedef PeriodicSCLattice<nodeid_t> SCLattice;
typedef PctccConfiguration<SCLattice> SC_conf;

///////////////////////////////////////////////////////////////////////////////
//
// spindirSC: Transform pointers to neighbours to directions (SC lattice)
// 

class spindirSC {
public:
  typedef PeriodicSCLattice<short>  SpinLattice_t;

  SpinLattice_t spinlat;

  spindirSC(SCLattice&);
  void update();
  double dot(short s1,short s2);
  short spin_component(int s,short mu);

private:
  static int dotprod_table[7][7];
  static short spin_component_table[7][3];
  SCLattice& reflat;
};

int spindirSC::dotprod_table[7][7];
short spindirSC::spin_component_table[7][3];

spindirSC::spindirSC(SCLattice& l) :
  reflat(l),
  spinlat(l.Xsize(),l.Ysize(),l.Zsize())
{
  memset(&dotprod_table,0,7*7*sizeof(int));
  dotprod_table[1][1]=1;
  dotprod_table[1][3]=dotprod_table[3][1]=-1;
  dotprod_table[2][2]=1;
  dotprod_table[2][4]=dotprod_table[4][2]=-1;
  dotprod_table[3][3]=1;
  dotprod_table[4][4]=1;
  dotprod_table[5][5]=1;
  dotprod_table[5][6]=dotprod_table[6][5]=-1;
  dotprod_table[6][6]=1;

  spin_component_table[0][0]=0;
  spin_component_table[0][0]=0;
  spin_component_table[0][0]=0;
  spin_component_table[1][0]=1;
  spin_component_table[1][1]=0;
  spin_component_table[1][2]=0;
  spin_component_table[2][0]=0;
  spin_component_table[2][1]=1;
  spin_component_table[2][2]=0;
  spin_component_table[3][0]=-1;
  spin_component_table[3][1]=0;
  spin_component_table[3][2]=0;
  spin_component_table[4][0]=0;
  spin_component_table[4][1]=-1;
  spin_component_table[4][2]=0;
  spin_component_table[5][0]=0;
  spin_component_table[5][1]=0;
  spin_component_table[5][2]=1;
  spin_component_table[6][0]=0;
  spin_component_table[6][1]=0;
  spin_component_table[6][2]=-1;

  update();
}

void spindirSC::update()
{
  SpinLattice_t::node_iterator s(spinlat);
  SCLattice::node_iterator rs(reflat);

  s.moveto(0,0,0);
  rs.moveto(0,0,0);
  for ( ; rs!=reflat.end(); ++rs, ++s) {
    if (*rs==SCLattice::nilnode) *s=0;
    else {
      int n=0;
      while ( *rs!=reflat.nodeid(&(rs.neighbour(n))) ) n++;
      assert(n<=5);
      *s=n+1;
    }
  }
}

inline double spindirSC::dot(short s1,short s2)
{
  return dotprod_table[s1][s2];
}

inline short spindirSC::spin_component(int s,short mu)
{
  return spin_component_table[s][mu];
}


///////////////////////////////////////////////////////////////////////////////
//
// Overlap
//

// Parameters

class OverlapSC_parameters : public OverlapSC_parameters_base {
public:
  OverlapSC_parameters();
};

OverlapSC_parameters::OverlapSC_parameters() :
  OverlapSC_parameters_base()
{
  ctrl_file_options.add_options()
    ("overlapSC.file_prefix",po::value<std::string>(),"Overlap file name prefix")
    ("overlapSC.step",po::value<int>(),"Overlap write step")
    ;
}

// Environment

class OverlapSC_environment : public OverlapSC_environment_base {
public:
  std::string overlap_fname;
  int         ov_step;

  OverlapSC_environment(int argc,char *argv[],OverlapSC_parameters &);
};

OverlapSC_environment::OverlapSC_environment(int argc,char *argv[],
					     OverlapSC_parameters& par) :
  OverlapSC_environment_base(argc,argv,par)
{
  overlap_fname=par.value("overlapSC.file_prefix").as<std::string>()+fin_infix+".dat";
  ov_step=par.value("overlapSC.step").as<int>();
}

// Overlap

template <typename environment>
class OverlapSC : public Observable {
private:
  typedef PctccSimulation<SC_conf,environment> Sim;

  Sim         &sim;
  SC_conf     &iconf;
  environment &ienv;
  spindirSC   conf,refconf;
  double      reftime;
  FILE        *of;

  void start(Simulation::run_mode);
  void do_observation();

  class overlap_per : public Persist_aid::obj_stream {
    spindirSC::SpinLattice_t &lat;
  public:
    overlap_per(const char* name,spindirSC::SpinLattice_t& lat_) : 
      Persist_aid::obj_stream(name), lat(lat_) {}
    void read(std::istream& f) {lat.read(f);}
    void write(std::ostream& f) {lat.write(f);}
  } *refconf_save;
  void register_for_persistence(Persist_aid&);

public:
  OverlapSC(Simulation &sim);
  ~OverlapSC() {fclose(of);}
} ;

template <typename environment>
OverlapSC<environment>::OverlapSC(Simulation &s) :
  Observable("Overlap observable",
   	     dynamic_cast<Sim&>(s).ienv,
   	     dynamic_cast<Sim&>(s).iconf,
   	     dynamic_cast<Sim&>(s).ienv.ov_step),
  sim(dynamic_cast<Sim&>(s)),
  iconf(sim.iconf),
  ienv(sim.ienv),
  conf(sim.iconf.lat),
  refconf(sim.iconf.lat),
  refconf_save(0)
{
  refconf.update();
  reftime=iconf.time;
}

template <typename environment>
void OverlapSC<environment>::start(Simulation::run_mode rmode)
{
  switch (rmode) {
  case Simulation::new_run:
    of=fopen(ienv.overlap_fname.c_str(),"w");
    assert(of);
    fprintf(of,"# Pctcc overlap, step: %d\n",ienv.ov_step);
    fprintf(of,"#     t-t_w     overlap \n");
    break;
  case Simulation::continuation_run:
    of=fopen(ienv.overlap_fname.c_str(),"a");
    assert(of);
    break;
  }
  Observable::start(rmode);
}

template <typename environment>
void OverlapSC<environment>::register_for_persistence(Persist_aid& persist)
{
  refconf_save = new overlap_per("OverlapSC.refconf",refconf.spinlat);
  persist.reg_var(refconf_save);
  persist.reg_var("OverlapSC.reftime",&reftime);
  Observable::register_for_persistence(persist);
}

template <typename environment>
void OverlapSC<environment>::do_observation()
{
  spindirSC::SpinLattice_t::node_iterator c(conf.spinlat);
  spindirSC::SpinLattice_t::node_iterator rc(refconf.spinlat);
  double Q=0;

  conf.update();
  c.moveto(0,0,0);
  rc.moveto(0,0,0);

  for ( ; rc!=refconf.spinlat.end(); ++rc, ++c) {
    Q+=conf.dot(*rc,*c);
  }

  Q/=iconf.vol;

  fprintf(of,"%g  %g\n",iconf.time-reftime,Q);
  fflush(of);
}

#endif /* _PCTCCOBS_HH_ */
