/* 
 * SkSC.hh -- Structure factor for the SC lattice
 *
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _SKSC_HH_
#define _SKSC_HH_

// Parameters

class SkSC_parameters : public SkSC_parameters_base {
public:
  SkSC_parameters();
};

SkSC_parameters::SkSC_parameters() :
  SkSC_parameters_base()
{
  ctrl_file_options.add_options()
    ("Sk.file_prefix",po::value<std::string>(),"S(k) file name prefix")
    ("Sk.step",po::value<int>(),"S(k) write step")
    ("Sk.miller_h",po::value<int>()->default_value(1),"Direction of wavevector: Miller index h")
    ("Sk.miller_k",po::value<int>()->default_value(1),"Direction of wavevector: Miller index k")
    ("Sk.miller_l",po::value<int>()->default_value(1),"Direction of wavevector: Miller index l")
    ;
}

// Environment

class SkSC_environment : public SkSC_environment_base {
public:
  std::string Sk_fname;
  int         Sk_step;
  int         miller[3];

  SkSC_environment(int argc,char *argv[],SkSC_parameters &);
};

SkSC_environment::SkSC_environment(int argc,char *argv[],
				   SkSC_parameters& par) :
  SkSC_environment_base(argc,argv,par)
{
  Sk_fname=par.value("Sk.file_prefix").as<std::string>()+fin_infix+".dat";
  Sk_step=par.value("Sk.step").as<int>();
  miller[0]=par.value("Sk.miller_h").as<int>();
  miller[1]=par.value("Sk.miller_k").as<int>();
  miller[2]=par.value("Sk.miller_l").as<int>();
}

// S(k)

template <typename environment>
class SkSC : public Observable {
private:
  typedef PctccSimulation<SC_conf,environment> Sim;

  Sim         &sim;
  SC_conf     &iconf;
  environment &ienv;
  spindirSC   conf;

  int         Nk;
  double      dk;         // dk es el modulo de los vectores de la base reciproca (la
                          // reciproca de la cubica es otra cubica con b=2pi/a)
  double      *Skr,*Ski;
  long        nsamp;

  FILE        *of;

  void start(Simulation::run_mode);
  void do_observation();
  double dotkrbase(int j,int k,int l);
  int pdiff(int i, int j, int L);
  void writeSk();

public:
  SkSC(Simulation &sim);
  ~SkSC();
} ;

template <typename environment>
SkSC<environment>::SkSC(Simulation &s) :
  Observable("Observable for S(k)",
   	     dynamic_cast<Sim&>(s).ienv,
   	     dynamic_cast<Sim&>(s).iconf,
   	     dynamic_cast<Sim&>(s).ienv.Sk_step),
  sim(dynamic_cast<Sim&>(s)),
  iconf(sim.iconf),
  ienv(sim.ienv),
  conf(sim.iconf.lat)
{
  Nk=std::max(conf.spinlat.Xsize(),conf.spinlat.Ysize());
  Nk=std::max(Nk,conf.spinlat.Zsize());
  dk=2*M_PI/Nk;         // La reciproca de la cubica es otra cubica con b=2pi/a
  Skr=new double[Nk];
  memset(Skr,0,Nk*sizeof(double));
  Ski=new double[Nk];
  memset(Ski,0,Nk*sizeof(double));
}

template <typename environment>
void SkSC<environment>::start(Simulation::run_mode rmode)
{
  switch (rmode) {
  case Simulation::new_run:
    of=fopen(ienv.Sk_fname.c_str(),"w");
    fprintf(of,"# S(k) in the direction (%d%d%d)\n",ienv.miller[0],ienv.miller[1],ienv.miller[2]);
    fprintf(of,"# time |k|   Re(S(k))  Im(S(k))\n");
    break;
  case Simulation::continuation_run:
    of=fopen(ienv.Sk_fname.c_str(),"a");
    break;
  }
  Observable::start(rmode);
}

template <typename environment>
void SkSC<environment>::writeSk()
{
  for (int ik=0; ik<Nk; ik++) {
    double kx=ik*ienv.miller[0]*dk;
    double ky=ik*ienv.miller[1]*dk;
    double kz=ik*ienv.miller[2]*dk;
    fprintf(of,"%g %g %g %g\n",iconf.time,sqrt(kx*kx+ky*ky+kz*kz),
	    Skr[ik]/(conf.spinlat.nNodes()),
	    Ski[ik]/(conf.spinlat.nNodes()));
  }
  fprintf(of,"\n\n");
  fflush(of);
}

template <typename environment>
SkSC<environment>::~SkSC()
{
  fclose(of);
  delete[] Skr;
  delete[] Ski;
}

template <typename environment>
inline double SkSC<environment>::dotkrbase(int j,int k,int l)
{
  return dk*(ienv.miller[0]*j + ienv.miller[1]*k + ienv.miller[2]*l);
}

// Esto viene de stack overflow
// http://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
// template <typename T> int sgn(T val) {
//     return (T(0) < val) - (val < T(0));
// }

// Esto viene de stack overflow
//http://stackoverflow.com/questions/3912112/check-if-a-number-is-non-zero-using-bitwise-operators-in-c
// inline int isNotZero(unsigned int n){ // unsigned is safer for bit operations
//   return ((n | (-n)) >> (4*sizeof(int)-1) ) & 1; // if (n!=0) then n or -n has the sign bit set
// }


// La implementacion mas rapida de la Kronecker parece ser con la comparacion
// incluso mas rapida que una tabla (hice la prueba con una 7x7)
template <typename T>
inline int kdelta(T a,T b)
{
  return a==b;
}

template <typename environment>
void SkSC<environment>::do_observation()
{
  memset(Skr,0,Nk*sizeof(double));
  memset(Ski,0,Nk*sizeof(double));
  conf.update();

  for (int ix=0; ix<conf.spinlat.Xsize(); ix++)
    for (int jx=0; jx<conf.spinlat.Xsize(); jx++) {

      int j=pdiff(ix,jx,conf.spinlat.Xsize());
      for (int iy=0; iy<conf.spinlat.Ysize(); iy++)
	for (int jy=0; jy<conf.spinlat.Ysize(); jy++) {

	  int k=pdiff(iy,jy,conf.spinlat.Ysize());
	  for (int iz=0; iz<conf.spinlat.Zsize(); iz++)
	    for (int jz=0; jz<conf.spinlat.Zsize(); jz++) {

	      int l=pdiff(iz,jz,conf.spinlat.Zsize());
	      double d=dotkrbase(j,k,l);
	      int SS= kdelta(conf.spinlat(ix,iy,iz),conf.spinlat(jx,jy,jz));
	      if (SS==0) continue;
	      for (int ik=0; ik<Nk; ++ik) {
		Skr[ik]+=SS*cos(ik*d);
		Ski[ik]+=SS*sin(ik*d);
	      }
	      
	    }
	}
    }
  writeSk();
}
	 
	 
template <typename environment>
inline int SkSC<environment>::pdiff(int i, int j, int L)
{
  return i-j-L*rint((double)(i-j)/L);
}

#endif /* _SKSC_HH_ */
