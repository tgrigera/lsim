/* 
 * sgcorr.hh -- Spin glass space correlation for PCTCC
 *
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _SGCORR_HH_
#define _SGCORR_HH_

// Parameters

class sgcorrSC_parameters : public sgcorrSC_parameters_base {
public:
  sgcorrSC_parameters();
};

sgcorrSC_parameters::sgcorrSC_parameters() :
  sgcorrSC_parameters_base()
{
  ctrl_file_options.add_options()
    ("sgcorrSC.file_prefix",po::value<std::string>(),"SG correlation file name prefix")
    ("sgcorrSC.step",po::value<int>(),"SG correlation write step")
    ;
}

// Environment

class sgcorrSC_environment : public sgcorrSC_environment_base {
public:
  std::string sgcorr_fname;
  int         sgcorr_step;

  sgcorrSC_environment(int argc,char *argv[],sgcorrSC_parameters &);
};

sgcorrSC_environment::sgcorrSC_environment(int argc,char *argv[],
					     sgcorrSC_parameters& par) :
  sgcorrSC_environment_base(argc,argv,par)
{
  sgcorr_fname=par.value("sgcorrSC.file_prefix").as<std::string>()+fin_infix+".dat";
  sgcorr_step=par.value("sgcorrSC.step").as<int>();
}

// Correlation

template <typename environment>
class sgcorrSC : public Observable {
private:
  typedef PctccSimulation<SC_conf,environment> Sim;

  Sim         &sim;
  SC_conf     &iconf;
  environment &ienv;
  spindirSC   conf;
  FILE        *of;
  double      *C,*Csg;
  long        Nsamp;

  void start(Simulation::run_mode);
  void do_observation();
  int pdiff(int i, int j, int L);
  void register_for_persistence(Persist_aid&);

public:
  sgcorrSC(Simulation &sim);
  ~sgcorrSC();
} ;

template <typename environment>
sgcorrSC<environment>::sgcorrSC(Simulation &s) :
  Observable("SG correlation observable",
   	     dynamic_cast<Sim&>(s).ienv,
   	     dynamic_cast<Sim&>(s).iconf,
   	     dynamic_cast<Sim&>(s).ienv.sgcorr_step),
  sim(dynamic_cast<Sim&>(s)),
  iconf(sim.iconf),
  ienv(sim.ienv),
  conf(sim.iconf.lat)
{
  C=new double[iconf.lat.Xsize()+1];  // Porque ddiff devuelve tanto +dmax como -dmax
  Csg=new double[iconf.lat.Xsize()+1];
  memset(C,0,(iconf.lat.Xsize()+1)*sizeof(double));
  memset(Csg,0,(iconf.lat.Xsize()+1)*sizeof(double));
  Nsamp=0;
}

template <typename environment>
void sgcorrSC<environment>::register_for_persistence(Persist_aid& persist)
{
  persist.reg_var("sgcorrSC.C",C,iconf.lat.Xsize()+1);
  persist.reg_var("sgcorrSC.Csg",Csg,iconf.lat.Xsize()+1);
  persist.reg_var("sgcorrSC.Nsamp",&Nsamp);
  Observable::register_for_persistence(persist);
}

template <typename environment>
void sgcorrSC<environment>::start(Simulation::run_mode rmode)
{
  switch (rmode) {
  case Simulation::new_run:
    break;
  case Simulation::continuation_run:
    break;
  }
  Observable::start(rmode);
}

template <typename environment>
sgcorrSC<environment>::~sgcorrSC()
{
  of=fopen(ienv.sgcorr_fname.c_str(),"w");
  assert(of);
  fprintf(of,"# SG correlation, step: %d\n",ienv.sgcorr_step);
  fprintf(of,"# r  C(r)  C_SG\n");
  C[iconf.lat.Xsize()]+=C[0];
  Csg[iconf.lat.Xsize()]+=Csg[0];
  for (int i=1; i<iconf.lat.Xsize()+1; i++)
    fprintf(of,"%d  %g  %g\n",i-iconf.lat.Xsize()/2,C[i]/Nsamp,Csg[i]/Nsamp);
  
  fclose(of);
  delete[] C;
  delete[] Csg;
}

template <typename environment>
void sgcorrSC<environment>::do_observation()
{
  conf.update();
  spindirSC::SpinLattice_t::node_iterator c(conf.spinlat);

  int si[3],sj[3];
  double C0=0,Ckmin_r=0,Ckmin_i=0;
  for (int ix=0; ix<conf.spinlat.Xsize(); ix++) {
    for (int iy=0; iy<conf.spinlat.Ysize(); iy++) {
      for (int iz=0; iz<conf.spinlat.Zsize(); iz++) {
	for (int jx=0; jx<conf.spinlat.Xsize(); jx++) {
	  int j=pdiff(ix,jx,conf.spinlat.Xsize()) + iconf.lat.Xsize()/2;

	  si[0]=conf.spin_component(conf.spinlat(ix,iy,iz),0);
	  si[1]=conf.spin_component(conf.spinlat(ix,iy,iz),1);
	  si[2]=conf.spin_component(conf.spinlat(ix,iy,iz),2);
	  sj[0]=conf.spin_component(conf.spinlat(jx,iy,iz),0);
	  sj[1]=conf.spin_component(conf.spinlat(jx,iy,iz),1);
	  sj[2]=conf.spin_component(conf.spinlat(jx,iy,iz),2);

	  int S0=si[0]*sj[0];
	  int S1=si[1]*sj[1];
	  int S2=si[2]*sj[2];
	  C[j]+=(double) (S0+S1+S2)/iconf.vol;
	  Csg[j]+=(double) (S0*S0+S1*S1+S2*S2)/iconf.vol;


	  // if (jj==-14 || jj==14) {
	  //   std::cout << "spin " <<ix<<' '<<iy<<' '<<iz<<"   comp "<<si[0]<<' ' <<si[1]<<' '<<si[2]<<" SS "<<SS<<" Corr[0]"<<Corr[iconf.lat.Xsize()/2]<<'\n';
	  //   std::cout << "spin " <<jx<<' '<<iy<<' '<<iz<<"   comp "<<sj[0]<<' ' <<sj[1]<<' '<<sj[2]<<" SS "<<SS<<" Corr[0]"<<Corr[j]<<'\n';
	  // }
	  


	  // S=si[1]*sj[1];
	  // SS+=S*S;
	  // S=si[2]*sj[2];
	  // SS+=S*S;
	  // SS/=3;

	}
      }
    }
  }

  Nsamp++;
}

template <typename environment>
inline int sgcorrSC<environment>::pdiff(int i, int j, int L)
{
  return i-j-L*rint((double)(i-j)/L);
}


#endif /* _SGCORR_HH_ */
