/* 
 * pctcc-crystal2.cc -- Generate a PCTCC crystal on the SQ lattice
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#include "glsim/errdeb.h"
#include "../graph/lattice2D.hh"
#include "pctcc.hh"

typedef PeriodicSQLattice<nodeid_t> SQLattice;
typedef PctccConfiguration<SQLattice> SQ_conf;

class SQParam : public PctccConfiguration_constructParam {
private:
  int Lx_,Ly_;
public:
  SQParam() {}
  SQParam& Lx(int L) {Lx_=L; return *this;}
  SQParam& Ly(int L) {Ly_=L; return *this;}

  friend class PctccConfiguration<SQLattice>;
} ;

template<>
PctccConfiguration<SQLattice>::
PctccConfiguration(const PctccConfiguration_constructParam &par) :
  Configuration("PCTCC on the SQ lattice"),
  lat(dynamic_cast<const SQParam&>(par).Lx_,
      dynamic_cast<const SQParam&>(par).Ly_)
{
  vol=lat.nNodes();
  deflt("");
}


void cryst(SQ_conf& conf)
{
  for (int y=0; y<conf.lat.Ysize(); ++y) {
    for (int x=0; x<conf.lat.Xsize(); ++x) {
      int a = (x + 2*y) % 5;
      switch (a) {
      case 0:
	conf.lat(x,y)=SQLattice::nilnode;
	break;
      case 1:
	conf.lat(x,y)=conf.lat.node_id_protected(x-1,y);
	break;
      case 2:
	conf.lat(x,y)=conf.lat.node_id_protected(x,y-1);
	break;
      case 3:
	conf.lat(x,y)=conf.lat.node_id_protected(x,y+1);
	break;
      case 4:
	conf.lat(x,y)=conf.lat.node_id_protected(x+1,y);
	break;
      }
    }
  }
}

random_number_generator RNG(gsl_rng_mt19937,0);

void check_config(SQ_conf &conf)
{
  bool ok=true;

  for (SQLattice::node_iterator s(conf.lat); s!=conf.lat.end(); s++) {
    if (*s==SQLattice::nilnode) continue;
    if (conf.lat.node_value(*s)!=SQLattice::nilnode) {
      ok=false;
      std::cout << "ERROR: node " << s.node_id() << " points to nonempty node " << *s << '\n';
    }
  }
  if (ok) std::cout << "Configuration " << conf.time << " checked OK\n";
}

int main(int argc,char *argv[])
{
  if (argc!=3) {
    std::cout << "usage: " << argv[0] << " L fname(output)\n";
    exit(1);
  }

  int L=atoi(argv[1]);
  char *fname=argv[2];

  SQ_conf conf(SQParam().Lx(L).Ly(L));
  cryst(conf);
  check_config(conf);
  conf.save(fname);
  conf.load(fname);
  check_config(conf);
}

