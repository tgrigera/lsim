/* 
 * pctcc/conf.hh -- Configuration for the PCTCC model
 *
 * This header declares the appropriate configuration class for
 * simulaiton of the lattice glass invented by Pica Ciamarra, Tarzia,
 * de Candia and Coniglio.  It is generic with respect of the
 * underlying graph, which is a template parameter.
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _PCTCC_CONF_H_
#define _PCTCC_CONF_H_

#include "glsim/configuration.hh"
#include "../graph/lattice2D.hh"
#include "../graph/lattice3D.hh"

template <typename Graph>
class PCTCCConf : public glsim::Configuration {
public:
  PCTCCConf(int a=0,int b=0,int c=0);

  typedef Graph Graph_t;
  Graph         lat;
  int           vol;
  double        time;

  void init(const char*);
  void load(const char*);
  void save(const char*);

  using Configuration::load;
  using Configuration::save;
  using Configuration::init;
} ;

template <typename Graph>
void PCTCCConf<Graph>::init(const char* fname)
{
  if (fname!=0) 
    load(fname);
  else {
    for (typename Graph::node_literator s=lat.begin(); s!=lat.end(); s++)
      *s=Graph::nilnode;
  }
  vol=lat.nNodes();
  time=0;
}

template <typename Graph>
void PCTCCConf<Graph>::save(const char* file)
{
  std::ofstream f(file,std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);
  if (!f.is_open())
    throw glsim::Open_file_error(file);
  f.write((char*)&time,sizeof(time));
  lat.write(f);
}

template <typename Graph>
void PCTCCConf<Graph>::load(const char* file)
{
  std::ifstream f(file,std::ios_base::in | std::ios_base::binary);
  if (!f.is_open())
    throw glsim::Open_file_error(file);
  f.read((char*)&time,sizeof(time));
  lat.read(f);
  vol=lat.nNodes();
}


typedef PeriodicSCLattice<nodeid_t>  SCLattice;
typedef PeriodicSQLattice<nodeid_t>  SQLattice;

template<> PCTCCConf<SCLattice>::PCTCCConf(int Lx,int Ly,int Lz) :
  Configuration("PCTCC on the SC lattice"),
  lat(Lx,Ly,Lz),
  time(0)
{
  vol=lat.nNodes();
  init();
}

template<> PCTCCConf<SQLattice>::PCTCCConf(int Lx,int Ly,int dum) :
  Configuration("PCTCC on the SQ lattice"),
  lat(Lx,Ly),
  time(0)
{
  vol=lat.nNodes();
  init();
}

#endif /* _PCTCC_CONF_H_ */
