/* 
 * pctcc/kmcenv.hh -- Environment for kinetic MC for the PCTCC model
 *
 * This contains the simulation class for single-spin-flip Metropolis
 * Monte Carlo of the lattice glass invented by Pica Ciamarra, Tarzia,
 * de Candia and Coniglio.
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _KMCENV_HH_
#define _KMCENV_HH_

///////////////////////////////////////////////////////////////////////////////
//
// Parameters

#include <glsim/parameters.hh>

namespace po=boost::program_options;

class MCParameters : public glsim::Parameters {
public:
  MCParameters(const char* scope=Parameters::default_scope);
} ;

MCParameters::MCParameters(const char* scope) :
  Parameters(scope)
{
  parm_file_options().add_options()
    ("pctcc.time",po::value<double>(),"Target final time")
    ("pctcc.GrandCanonical",po::value<bool>()->default_value(false),"Allow grand canonical moves")
    ("pctcc.Lx",po::value<int>()->default_value(10),"")
    ("pctcc.Ly",po::value<int>()->default_value(10),"")
    ("pctcc.Lz",po::value<int>()->default_value(10),"")
    ("pctcc.alpha0",po::value<double>()->default_value(0.),"mu/kT (initial)")
    ("pctcc.alphar",po::value<double>()->default_value(0.),"d log(kT/mu)/dt")
    ;
}

///////////////////////////////////////////////////////////////////////////////
//
// Environment

#include <glsim/environment.hh>

class KMCEnvironment : public glsim::CTSimEnvironment {
public:
  KMCEnvironment(const char* scope=glsim::Parameters::default_scope);

  // Set from .ini
  double time_requested;
  bool   GrandCanonical;
  int    Lx,Ly,Lz;
  double alpha0,alphar;

  // Internal
  double alpha;
  int    nparts;

  // For observation
  double last_alpha;
  int    last_nparts;
protected:
  void init_local();
  void warm_init_local();

private:
  MCParameters par;

  // Serialization
  friend class ::boost::serialization::access;
  template <typename Archive>
  void serialize(Archive &ar,const unsigned int version);
  virtual void vserial(iarchive_t &ar) {ar >> *this;}
  virtual void vserial(oarchive_t &ar) {ar << *this;}
} ;

KMCEnvironment::KMCEnvironment(const char* scope) :
  CTSimEnvironment(scope),
  GrandCanonical(false),
  Lx(5), Ly(5), Lz(5),
  alpha0(0.),
  alphar(0.),
  alpha(0.),
  nparts(0),
  last_alpha(0.),
  last_nparts(0)
{}

void KMCEnvironment::init_local()
{
  SimEnvironment::init_local();
  time_requested=par.value("pctcc.time").as<double>();
  GrandCanonical=par.value("pctcc.GrandCanonical").as<bool>();
  Lx=par.value("pctcc.Lx").as<int>();
  Ly=par.value("pctcc.Ly").as<int>();
  Lz=par.value("pctcc.Lz").as<int>();
  alpha0=par.value("pctcc.alpha0").as<double>();
  alpha=alpha0;
  alphar=par.value("pctcc.alphar").as<double>();
}

void KMCEnvironment::warm_init_local()
{
  SimEnvironment::warm_init_local();
  time_requested=par.value("pctcc.time").as<double>();
  GrandCanonical=par.value("pctcc.GrandCanonical").as<bool>();
  alphar=par.value("pctcc.alphar").as<double>();
}


template<typename Archive>
inline void KMCEnvironment::serialize(Archive &ar,const unsigned int version)
{
  ar & boost::serialization::base_object<glsim::SimEnvironment>(*this);
  ar & time_requested;
  ar & GrandCanonical;
  ar & Lx & Ly & Lz;
  ar & alpha0 & alphar & alpha;
  ar & nparts & last_alpha & last_nparts;
}


#endif /* _KMCENV_HH_ */
