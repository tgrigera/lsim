/* 
 * pctcc/mc.hh -- Standard MC for the PCTCC model
 *
 * This contains the simulation class for single-spin-flip Metropolis
 * Monte Carlo of the lattice glass invented by Pica Ciamarra, Tarzia,
 * de Candia and Coniglio.
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _MC_HH_
#define _MC_HH_

#include <glsim/simulation.hh>
#include <glsim/random.hh>

template <typename Configuration>
class PctccSimulation : public glsim::Simulation {
public:
  PctccSimulation(MCEnvironment&,Configuration&);

  const char *name() const;
  void step();
  void log_start_sim(),log_stop_sim();
  void log();

  MCEnvironment &ienv;
  Configuration &iconf;

  bool check_config();

private:
  long trymov,accmov,trycrdes,acccrdes;
  double alpha_micro_rate;

  glsim::uniform_real ran;

  void count_parts();
} ;

template <typename Configuration>
PctccSimulation<Configuration>::PctccSimulation(MCEnvironment &env,
						Configuration &conf) :
  Simulation(env,conf),
  ienv(env),
  iconf(conf),
  trymov(0),
  accmov(0),
  trycrdes(0),
  acccrdes(0),
  ran()
{
  count_parts();
  alpha_micro_rate=ienv.alphar/iconf.lat.nNodes();
}

template <typename Configuration>
void PctccSimulation<Configuration>::count_parts()
{
  ienv.nparts=0;
  for (typename Configuration::Graph_t::node_literator s=iconf.lat.begin();
       s!=iconf.lat.end(); ++s)
    if (*s!=Configuration::Graph_t::nilnode) ienv.nparts++;
}

template <typename Configuration>
const char* PctccSimulation<Configuration>::name() const
{
  return "Metropolis MC of the Pica Ciamarra - Tarzia - de Candia - Coniglio model";
}

///////////////////////////////////////////////////////////////////////////////
//
// step

template <typename Configuration>
void PctccSimulation<Configuration>::step()
{
  typename Configuration::Graph_t& lat=iconf.lat;
  static typename Configuration::Graph_t::node_iterator site(lat),newneighb(lat);
  typename Configuration::Graph_t::node_literator fsite;
  nodeid_t old;

  for (int itry=0; itry<lat.nNodes(); itry++) {

    // We do motion AND creaton/destruction (as in PCTCC paper)

    // Motion
    site.random();
    if (*site==Configuration::Graph_t::nilnode) goto grand_canonical; // site empty
    trymov++;
    fsite=site;
    site.random_neighbour();
    if (*site!=Configuration::Graph_t::nilnode) goto grand_canonical; // site occupied; move failed
    old=*fsite;
    *fsite=Configuration::Graph_t::nilnode;
    if (find_in_neighbours(site,site.node_id())!=-1) {*fsite=old; goto grand_canonical;}
    newneighb=site;
    newneighb.random_neighbour();
    if (*newneighb!=Configuration::Graph_t::nilnode) *fsite=old;
    else {
      *site=newneighb.node_id();  // move suceeded
      accmov++;
    }

    // Creation/destruction
  grand_canonical:
    if (!ienv.GrandCanonical) continue;
    trycrdes++;
    site.random();
    if (*site==Configuration::Graph_t::nilnode) {  // Create particle
      if (find_in_neighbours(site,site.node_id())!=-1) continue;  // site is pointed at
      fsite=site;
      site.random_neighbour();
      if (*site!=Configuration::Graph_t::nilnode) continue;  // site occupied; failed
      // compute exp(alpha*(Nnew-Nold))=exp(alpha)
      if (ienv.alpha>=0 || ran()<exp(ienv.alpha)) { // accept
	*fsite=site.node_id();
	ienv.nparts++;
	acccrdes++;
      }
    } else {
      if (ran()<1./site.nNeighbours())  	// add prob for detailed balance
	// compute exp(alpha*(Nnew-Nold))=exp(-alpha)
	if (ienv.alpha<0 || ran()<exp(-ienv.alpha) ) {
	  *site=Configuration::Graph_t::nilnode;
	  ienv.nparts--;
	  acccrdes++;
	}
    }

    ienv.alpha+=ienv.alpha*alpha_micro_rate;

  }
  iconf.time+=1.;
  ienv.run_completed=ienv.steps_in_run+1>=ienv.requested_steps;
}

template <typename Configuration>
bool PctccSimulation<Configuration>::check_config()
{
  bool ok=true;

  for (typename Configuration::Graph_t::node_iterator s(iconf.lat); s!=iconf.lat.end(); s++) {
    if (*s==Configuration::Graph_t::nilnode) continue;
    if (iconf.lat.node_value(*s)!=Configuration::Graph_t::nilnode) {
      ok=false;
      std::cout << "ERROR: node " << s.node_id() << " points to nonempty node " << *s << '\n';
    }
  }
  return ok;
}


///////////////////////////////////////////////////////////////////////////////
//
// logging

template <typename Configuration>
void PctccSimulation<Configuration>::log_start_sim()
{
  glsim::logs(glsim::info) << "\n\n" << name() << "\n\n";
  glsim::logs(glsim::info) << "Previously completed steps = " << ienv.steps_completed << '\n';
  if (check_config()) glsim::logs(glsim::info) <<
			"Configuration at time " << iconf.time << " checked OK\n";
  else exit(1);
  glsim::logs(glsim::info) << "   Steps  Nparts Density alpha  T/mu   accmov  acccrdes\n";
}

template <typename Configuration>
void PctccSimulation<Configuration>::log()
{
  char buff[300];
  sprintf(buff,"%8.4g %6d %7.4f %4.3f %4.3f %8.3e %8.3e\n",iconf.time,ienv.nparts,(double) ienv.nparts/iconf.vol,
	 ienv.alpha,1./ienv.alpha,(double)accmov/trymov,(double)acccrdes/trycrdes);
  glsim::logs(glsim::info) << buff;
}

template <typename Configuration>
void PctccSimulation<Configuration>::log_stop_sim()
{
  if (check_config())
    glsim::logs(glsim::info) << "Configuration at time " << iconf.time << " checked OK\n";
}


///////////////////////////////////////////////////////////////////////////////
//
// Observables

#include <glsim/observable.hh>

class PctccObsParameters : public glsim::Parameters {
public:
  PctccObsParameters(const char* scope=Parameters::default_scope);
} ;

PctccObsParameters::PctccObsParameters(const char* scope) :
  Parameters(scope)
{
  parm_file_options().add_options()
    ("pctcc.obs_file_prefix",po::value<std::string>()->default_value("obs"),"observation file prefix")
    ("pctcc.obs_interval",po::value<int>()->default_value(0))
    ;
}


template <typename Latt>
class PctccObs : public glsim::SBObservable {
public:
  PctccObs(MCEnvironment&,PCTCCConf<Latt>&);

  void interval_and_file();
  void write_header(),observe();

private:
  PctccObsParameters par;
  MCEnvironment      &env;
  PCTCCConf<Latt>    &conf;
} ;

template <typename Latt>
PctccObs<Latt>::PctccObs(MCEnvironment& e,PCTCCConf<Latt>& c) :
  SBObservable(e),
  par(e.scope()),
  env(e),
  conf(c)
{}

template <typename Latt>
void PctccObs<Latt>::interval_and_file()
{
  obs_file_prefix=par.value("pctcc.obs_file_prefix").as<std::string>();
  obs_interval=par.value("pctcc.obs_interval").as<int>();
}

template <typename Latt>
void PctccObs<Latt>::write_header()
{
  fprintf(of,"# Pctcc standard observer, interval: %d\n",obs_interval);
  fprintf(of,"#       Time     T/mu Nparts Density   SpVol\n");
}

template <typename Latt>
void PctccObs<Latt>::observe()
{
  double dens=(double) env.nparts/conf.vol;
  double spvol= env.nparts== 0 ? 0 : 1./dens;

  fprintf(of,"%12.8g %8.4f %6d %7.4f %7.4f\n",conf.time,1./env.alpha,env.nparts,dens,spvol);
}

#endif /* _MC_HH_ */
