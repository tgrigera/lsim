/* 
 * pctcc/mcenv.hh -- Environment for standard MC for the PCTCC model
 *
 * This contains the simulation class for single-spin-flip Metropolis
 * Monte Carlo of the lattice glass invented by Pica Ciamarra, Tarzia,
 * de Candia and Coniglio.
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _MCENV_HH_
#define _MCENV_HH_

///////////////////////////////////////////////////////////////////////////////
//
// Parameters

#include <glsim/parameters.hh>

namespace po=boost::program_options;

class MCParameters : public glsim::Parameters {
public:
  MCParameters(const char* scope=Parameters::default_scope);
} ;

MCParameters::MCParameters(const char* scope) :
  Parameters(scope)
{
  parm_file_options().add_options()
    ("pctcc.steps",po::value<int>(),"Number of steps")
    ("pctcc.GrandCanonical",po::value<bool>()->default_value(false),"Allow grand canonical moves")
    ("pctcc.Lx",po::value<int>()->default_value(10),"")
    ("pctcc.Ly",po::value<int>()->default_value(10),"")
    ("pctcc.Lz",po::value<int>()->default_value(10),"")
    ("pctcc.alpha0",po::value<double>()->default_value(0.),"mu/kT (initial)")
    ("pctcc.alphar",po::value<double>()->default_value(0.),"d log(kT/mu)/dt")
    ;
}

///////////////////////////////////////////////////////////////////////////////
//
// Environment

#include <glsim/environment.hh>

class MCEnvironment : public glsim::SimEnvironment {
public:
  MCEnvironment(const char* scope=glsim::Parameters::default_scope);

  // Set from .ini
  int    requested_steps;
  bool   GrandCanonical;
  int    Lx,Ly,Lz;
  double final_time;
  double alpha0,alphar;

  // Internal
  double alpha;
  int    nparts;

protected:
  void init_local();
  void warm_init_local();

private:
  MCParameters par;

  // Serialization
  friend class ::boost::serialization::access;
  template <typename Archive>
  void serialize(Archive &ar,const unsigned int version);
  virtual void vserial(iarchive_t &ar) {ar >> *this;}
  virtual void vserial(oarchive_t &ar) {ar << *this;}
} ;

MCEnvironment::MCEnvironment(const char* scope) :
  SimEnvironment(scope),
  requested_steps(0),
  GrandCanonical(false),
  Lx(5), Ly(5), Lz(5),
  alpha0(0.),
  alphar(0.)
{}

void MCEnvironment::init_local()
{
  SimEnvironment::init_local();
  requested_steps=par.value("pctcc.steps").as<int>();
  GrandCanonical=par.value("pctcc.GrandCanonical").as<bool>();
  Lx=par.value("pctcc.Lx").as<int>();
  Ly=par.value("pctcc.Ly").as<int>();
  Lz=par.value("pctcc.Lz").as<int>();
  alpha0=par.value("pctcc.alpha0").as<double>();
  alpha=alpha0;
  alphar=par.value("pctcc.alphar").as<double>();
}

void MCEnvironment::warm_init_local()
{
  SimEnvironment::warm_init_local();
  requested_steps=par.value("pctcc.steps").as<int>();
  GrandCanonical=par.value("pctcc.GrandCanonical").as<bool>();
  alphar=par.value("pctcc.alphar").as<double>();
}


template<typename Archive>
inline void MCEnvironment::serialize(Archive &ar,const unsigned int version)
{
  ar & boost::serialization::base_object<glsim::SimEnvironment>(*this);
  ar & requested_steps;
  ar & GrandCanonical;
  ar & Lx & Ly & Lz;
  ar & alpha0 & alphar & alpha;
}

#endif /* _MCENV_HH_ */
