/* 
 * ad1sSQ.cc - Monte Carlo simulation of anihilation-diffusion reaction
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 * 
 */

#include "glsim/errdeb.h"
#include "glsim/mc_parameters.hh"
#include "glsim/mc_environment.hh"
#include "../graph/lattice2D.hh"
#include "ad1s.hh"

class AD1SSQ_parameters : public MonteCarlo_parameters {
public:
  AD1SSQ_parameters();
} ;

AD1SSQ_parameters::AD1SSQ_parameters() :
MonteCarlo_parameters()
{
  ctrl_file_options.add_options()
    ("AD1S.Lx",po::value<int>()->default_value(10),"")
    ("AD1S.Ly",po::value<int>()->default_value(10),"")
    ("AD1S.rho_ini",po::value<double>()->default_value(0.5))
    ;
}

class AD1SSQ_environment : public MonteCarlo_environment {
public:
  int    Lx,Ly,Lz;
  double rho_ini;

  AD1SSQ_environment(const char* efile_read="environment_ini.dat",
		   const char* efile_write="environment_fin.dat",
		   const char* cfile_read="configuration_ini.dat",
		   const char* cfile_write="configuration_fin.dat");
  AD1SSQ_environment(int argc,char *argv[],AD1SSQ_parameters &par);

private:
  void register_vars();
} ;

AD1SSQ_environment::AD1SSQ_environment(int argc,char *argv[],AD1SSQ_parameters &par) :
  MonteCarlo_environment(argc,argv,par)
{
  Lx=par.value("AD1S.Lx").as<int>();
  Ly=par.value("AD1S.Ly").as<int>();
  rho_ini=par.value("AD1S.rho_ini").as<double>();
  register_vars();
}

AD1SSQ_environment::AD1SSQ_environment(const char* efr,const char* efw,const char* cfr,
				       const char* cfw) :
  MonteCarlo_environment(efr,efw,cfr,cfw),
  Lx(10),
  Ly(10),
  rho_ini(0.5)
{
  register_vars();
}

void AD1SSQ_environment::register_vars()
{
  persist.reg_var("AD1SSQ_environment.Lx",&Lx,pa::write_only);
  persist.reg_var("AD1SSQ_environment.Ly",&Lx,pa::write_only);
  persist.reg_var("AD1SSQ_environment.rho_ini",&Lx,pa::write_only);
}


///////////////////////////////////////////////////////////////////////////////
//
// configuration
//

typedef PeriodicSQLattice<int> SQLattice;

class AD1SSQ_conf : public Configuration {
public:
  typedef SQLattice Graph_t;

  AD1SSQ_conf(int Lx,int Ly,double rho_def);
  SQLattice  lat;
  std::vector<nodeid_t> plist;

  int        vol;
  double     time;

  void deflt(const char*);
  void load(const char*);
  void save(const char*);
  void check();
  void rebuild_plist();

private:
  double rho_default;
} ;

AD1SSQ_conf::AD1SSQ_conf(int Lx,int Ly,double rd) :
  Configuration("AD on the SQ lattice"),
  lat(Lx,Ly),
  rho_default(rd)
{
}

void AD1SSQ_conf::deflt(const char* fname)
{
  typename SQLattice::node_iterator site(lat);

  if (fname!=0) {
    load(fname);
    time=0;
    return;
  }
  vol=lat.nNodes();
  time=0;
  for (nodeid_t in=0; in<lat.nNodes(); in++) lat.node_ref(in)=-1;
  plist.reserve(vol*rho_default);
  while (plist.size()<vol*rho_default) {
    site.random();
    if (*site==-1) {
      *site=plist.size();
      plist.push_back(site.node_id());
    }
  }
}

void AD1SSQ_conf::save(const char* file)
{
  std::ofstream f(file,std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);
  f.write((char*)&time,sizeof(time));
  lat.write(f);
}

void AD1SSQ_conf::load(const char* file)
{
  std::ifstream f(file,std::ios_base::in | std::ios_base::binary);
  f.read((char*)&time,sizeof(time));
  lat.read(f);
  vol=lat.nNodes();
  rebuild_plist();
}

void AD1SSQ_conf::rebuild_plist()
{
  plist.clear();
  for (nodeid_t in=0; in<lat.nNodes(); in++)
    if (lat.node_ref(in)!=-1) {
      lat.node_ref(in)=plist.size();
      plist.push_back(in);
    }
}

void AD1SSQ_conf::check()
{
  for (int ip=0; ip<plist.size(); ip++) {
    if (lat.node_ref(plist[ip])!=ip) {
      std::cerr << "Bad conf\n";
      exit(1);
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

Parameters *create_parameters()
{
  return new AD1SSQ_parameters();
}

Environment *create_environment(int argc,char *argv[],Parameters *par)
{
  return new AD1SSQ_environment(argc,argv,*dynamic_cast<AD1SSQ_parameters*>(par));
}

Configuration *create_configuration(Environment *env)
{
  AD1SSQ_environment *e=dynamic_cast<AD1SSQ_environment*>(env);
  return new AD1SSQ_conf(e->Lx,e->Ly,e->rho_ini);
}

Simulation *create_simulation(Environment *env,Configuration *conf)
{
  typedef AD1S<AD1SSQ_conf,AD1SSQ_environment> Sim;
  AD1SSQ_environment *e=dynamic_cast<AD1SSQ_environment*>(env);
  AD1SSQ_conf *c=dynamic_cast<AD1SSQ_conf*>(conf);

  Sim *sim=new Sim(*e,*c);
  if (e->obs_step>0)
    sim->add_obs(new AD1S_standard_observable<AD1SSQ_conf,AD1SSQ_environment>(*sim));
  return sim;
}
