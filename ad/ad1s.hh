/* 
 * ad1s.hh - Classes for Monte Carlo of anihilation-diffusion
 *           reactions
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 * 
 */

#ifndef _AD1S_HH_
#define _AD1S_HH_

#include "glsim/simulation.hh"
#include "glsim/observable.hh"
#include "../graph/graph.hh"

template <typename configuration,typename environment>
class AD1S_standard_observable;

template <typename configuration,typename environment>
class AD1S : public Simulation {
public:
  AD1S(environment&,configuration&);
  void init_sim(run_mode mode);
  void step();
  void log(),log_start_run();
  const char* name();

  friend class AD1S_standard_observable<configuration,environment>;

private:
  typedef typename configuration::Graph_t::node_iterator iterator_t;
  typedef typename configuration::Graph_t::node_literator literator_t;

  configuration &conf;
  environment   &env;
} ;

template <typename configuration,typename environment>
AD1S<configuration,environment>::AD1S(environment& e,configuration& c) :
  Simulation(e,c),
  conf(c),
  env(e)
{
}

template <typename configuration,typename environment>
const char* AD1S<configuration,environment>::name()
{
  return "Anihilation-diffussion simulation, one component";
}

template <typename configuration,typename environment>
void AD1S<configuration,environment>::init_sim(run_mode mode)
{
}

template <typename configuration,typename environment>
void AD1S<configuration,environment>::step()
{
  typename configuration::Graph_t::node_iterator site(conf.lat);

  int nparts=conf.plist.size();
  conf.check();
  conf.rebuild_plist();

  for (int itry=0; itry<nparts; itry++) {
    int iw;
    do iw=env.ran[conf.plist.size()];
    while (conf.plist[iw]==-1);
    nodeid_t onode=conf.plist[iw];
    site.moveto(onode);
    site.random_neighbour();
    if (*site==-1) {
      *site=iw;
      conf.plist[iw]=site.node_id();
    } else {
      conf.plist[*site]=-1;
      conf.plist[iw]=-1;
      *site=-1;
      nparts-=2;
    }
    conf.lat.node_ref(onode)=-1;
  }

  // cleanup plist
  int last=0;
  for (int iter=0; iter!=conf.plist.size(); ++iter) {
    if (conf.plist[iter] != -1) {
      conf.plist[last] = conf.plist[iter];
      conf.lat.node_ref(conf.plist[last])=last;
      ++last;
    }
  }
  conf.plist.erase(conf.plist.begin()+last,conf.plist.end());
  // conf.rebuild_plist();
  // std::cerr << "After cleanup " << conf.plist.size() << " nparts " << nparts << '\n';

  conf.time+=1.;
}

template <typename configuration,typename environment>
void AD1S<configuration,environment>::log_start_run()
{
  double dens=(double) conf.plist.size()/conf.vol;
  printf("        Time   Nparts Density\n");
  printf("%12.8g %8d %8.4f\n",conf.time,conf.plist.size(),dens);
}

template <typename configuration,typename environment>
void AD1S<configuration,environment>::log()
{
  double dens=(double) conf.plist.size()/conf.vol;

  printf("%12.8g %8d %8.4f\n",conf.time,conf.plist.size(),dens);
}

///////////////////////////////////////////////////////////////////////////////
//
// observable
//

template <typename configuration,typename environment>
class AD1S_standard_observable : public Observable {
private:
  AD1S<configuration,environment>    &sim;
  environment                        &env;
  configuration                      &conf;
  FILE                               *of;

  void start(Simulation::run_mode);
  void do_observation();

public:
  AD1S_standard_observable(Simulation &sim);
  ~AD1S_standard_observable() {fclose(of);}
} ;

template <typename configuration,typename environment>
AD1S_standard_observable<configuration,environment>::AD1S_standard_observable(Simulation &s) :
  Observable("Pctcc standard observables",
	     dynamic_cast<AD1S<configuration,environment>&>(s).env,
	     dynamic_cast<AD1S<configuration,environment>&>(s).conf,
	     dynamic_cast<AD1S<configuration,environment>&>(s).env.obs_step),
  sim(dynamic_cast<AD1S<configuration,environment>&>(s)),
  conf(sim.conf),
  env(sim.env)
{
}

template <typename configuration,typename environment>
void AD1S_standard_observable<configuration,environment>::start(Simulation::run_mode rmode)
{
  switch (rmode) {
  case Simulation::new_run:
    of=fopen(sim.env.default_observation_file.c_str(),"w");
    assert(of);
    fprintf(of,"# AD standard observer, step: %d\n",step);
    fprintf(of,"#        Time   Nparts Density\n");
    break;
  case Simulation::continuation_run:
    of=fopen(sim.env.default_observation_file.c_str(),"a");
    assert(of);
    break;
  }
  Observable::start(rmode);
}

template <typename configuration,typename environment>
void AD1S_standard_observable<configuration,environment>::do_observation()
{
  double dens=(double) conf.plist.size()/conf.vol;

  fprintf(of,"%12.8g %8d %8.4f\n",conf.time,conf.plist.size(),dens);
}


#endif /* _AD1S_HH_ */
