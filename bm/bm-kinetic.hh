/* 
 * bm-kinetic.hh -- Kinetic MC for the Biroli-Mezard model
 *
 * This contains the simulation class for single-spin-flip Metropolis
 * Monte Carlo of the lattice glass invented by Biroli and Mezard, on
 * the simple cubic lattice.
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef _BM_KINETIC_HH_
#define _BM_KINETIC_HH_

#include <math.h>

#include "glsim/mc_parameters.hh"
#include "glsim/mc_environment.hh"
#include "glsim/simulation.hh"
#include "glsim/observable.hh"
#include "../graph/lattice3D.hh"

/******************************************************************************
 *
 * Parameters
 *
 */

class BMSCParameters : public MonteCarlo_parameters {
public:
  BMSCParameters();
} ;

BMSCParameters::BMSCParameters() :
  MonteCarlo_parameters()
{
  ctrl_file_options.add_options()
    ("BM.GrandCanonical",po::value<bool>()->default_value(false),"Allow grand canonical moves")
    ("BM.OnlyGrandCanonical",po::value<bool>()->default_value(false),"Allow ONLY create/anihilate moves")
    ("BM.ntypes",po::value<int>()->default_value(1),"")
    ("BM.Lx",po::value<int>()->default_value(10),"")
    ("BM.Ly",po::value<int>()->default_value(10),"")
    ("BM.Lz",po::value<int>()->default_value(10),"")
    ("BM.final_time",po::value<double>(),"target time")
    ("BM.alphadot",po::value<double>()->default_value(0.),"rate of change of alpha")
    ("BM.alpha1",po::value<double>()->default_value(1.),"mu for type 1")
    ("BM.alpha2",po::value<double>()->default_value(1.),"mu for type 2")
    ("BM.alpha3",po::value<double>()->default_value(1.),"mu for type 3")
    ("BM.alpha4",po::value<double>()->default_value(1.),"mu for type 4")
    ;
}

/******************************************************************************
 *
 * Configuration
 *
 */

struct SiteT {
  unsigned  nneighb : 4;
  unsigned  type    : 4;
} ;

class BMSCConfiguration : public Configuration {
public:
  typedef PeriodicSCLattice<SiteT> GraphT;

  GraphT  lat;
  int     vol;
  double  time;

  BMSCConfiguration(int Lx,int Ly,int Lz);
  void deflt(const char*);
  void load(const char*);
  void save(const char*);
} ;

BMSCConfiguration::BMSCConfiguration(int Lx,int Ly,int Lz) :
  Configuration("BM on the SC lattice"),
  lat(Lx,Ly,Lz)
{
  vol=lat.nNodes();
  deflt(0);
}

void BMSCConfiguration::deflt(const char* fname)
{
  if (fname!=0) 
    load(fname);
  else {
    for (GraphT::node_literator s=lat.begin(); s!=lat.end(); s++)
      s->nneighb=s->type=0;
  }
  vol=lat.nNodes();
  time=0;
}

void BMSCConfiguration::save(const char* file)
{
  std::ofstream f(file,std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);
  f.write((char*)&time,sizeof(time));
  lat.write(f);
}

void BMSCConfiguration::load(const char* file)
{
  std::ifstream f(file,std::ios_base::in | std::ios_base::binary);
  f.read((char*)&time,sizeof(time));
  lat.read(f);
  vol=lat.nNodes();
}

/******************************************************************************
 *
 * Environment
 *
 */

class BMSCEnvironment : public MonteCarlo_environment {
public:
  bool   GrandCanonical,OnlyGrandCanonical;
  int    Lx,Ly,Lz;
  int    ntypes;
  double final_time;
  double T,alphadot,alpha0[5],alpha[5];

  BMSCEnvironment(const char* efile_read="environment_ini.dat",
		   const char* efile_write="environment_fin.dat",
		   const char* cfile_read="configuration_ini.dat",
		   const char* cfile_write="configuration_fin.dat");
  BMSCEnvironment(int argc,char *argv[],BMSCParameters &par);

private:
  void register_vars();
} ;

BMSCEnvironment::BMSCEnvironment(int argc,char *argv[],BMSCParameters &par) :
  MonteCarlo_environment(argc,argv,par)
{
  GrandCanonical=par.value("BM.GrandCanonical").as<bool>();
  OnlyGrandCanonical=par.value("BM.OnlyGrandCanonical").as<bool>();
  Lx=par.value("BM.Lx").as<int>();
  Ly=par.value("BM.Ly").as<int>();
  Lz=par.value("BM.Lz").as<int>();
  T=temperature;
  alphadot=par.value("BM.alphadot").as<double>();
  ntypes=par.value("BM.ntypes").as<int>();
  final_time=par.value("BM.final_time").as<double>();
  alpha[1]=alpha0[1]=par.value("BM.alpha1").as<double>();
  alpha[2]=alpha0[2]=par.value("BM.alpha2").as<double>();
  alpha[3]=alpha0[3]=par.value("BM.alpha3").as<double>();
  alpha[4]=alpha0[4]=par.value("BM.alpha4").as<double>();
  register_vars();
}

BMSCEnvironment::BMSCEnvironment(const char* efr,const char* efw,const char* cfr,
				 const char* cfw) :
  MonteCarlo_environment(efr,efw,cfr,cfw),
  Lx(10),
  Ly(10),
  Lz(10),
  ntypes(1)
{
  alpha[1]=1;
  register_vars();
}

void BMSCEnvironment::register_vars()
{
  persist.reg_var("BMSCEnvironment.Lx",&Lx,pa::write_only);
  persist.reg_var("BMSCEnvironment.Ly",&Ly,pa::write_only);
  persist.reg_var("BMSCEnvironment.Lz",&Lz,pa::write_only);
  persist.reg_var("BMSCEnvironment.T",&T,pa::write_only);
  persist.reg_var("BMSCEnvironment.alphadot",&alphadot,pa::write_only);
  persist.reg_var("BMSCEnvironment.ntypes",&ntypes,pa::write_only);
  persist.reg_var("BMSCEnvironment.alpha01",alpha0+1,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha02",alpha0+2,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha03",alpha0+3,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha04",alpha0+4,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha1",alpha+1,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha2",alpha+2,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha3",alpha+3,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha4",alpha+4,pa::read_write);
}


/*******************************************************************************
 *
 * List of allowed moves
 *
 */
class ListofMoves {
private:
  bool               GrandCanonical,OnlyGrandCanonical;
  BMSCConfiguration& iconf;
  int                ntypes;
  typedef typename BMSCConfiguration::GraphT::node_t node_t;
  
  typename BMSCConfiguration::GraphT::node_iterator siteit;

  class substract_n : public UnaryFunction<SiteT> {
    void operator()(SiteT& s) {s.nneighb--;}
  } subs_neigh;
  class add_n : public UnaryFunction<SiteT> {
    void operator()(SiteT& s) {s.nneighb++;}
  } add_neigh;

public:
  struct movedesc_trans {
    node_t   *from,*to;

    movedesc_trans(node_t *f,node_t *t) :
      from(f), to(t) {}

  } ;
  struct movedesc_create {
    node_t   *dest;

    movedesc_create(node_t *d) :
      dest(d) {}
  } ;
  typedef std::vector<movedesc_trans> translist_t;
  typedef std::vector<std::vector<movedesc_create> > createlist_t;
  typedef std::vector<std::vector<node_t*> > occlist_t;
  translist_t moves_trans;
  createlist_t moves_create;
  occlist_t occ_sites;

  ListofMoves(bool GrandCan,bool OnlyGC,BMSCConfiguration& c,int ntyp_) :
    GrandCanonical(GrandCan), OnlyGrandCanonical(OnlyGC),
    ntypes(ntyp_), iconf(c), siteit(iconf.lat) {}
  int  Ntrans() const {return moves_trans.size();}
  int  Ncreate(int type) const {return moves_create[type].size();}
  int  Ndestroy(int type) const {return occ_sites[type].size();}
  void clear_moves();
  void build_move_list();
  void do_move_trans(int),do_move_create(int,int),do_move_destroy(int,int);

private:
  void add_moves_with_dest(const typename BMSCConfiguration::GraphT::node_iterator &site);
  void add_creations_at(const typename BMSCConfiguration::GraphT::node_iterator &site);

  //  friend std::ostream& operator<< <>(std::ostream&,ListofMoves&);
} ;

void ListofMoves::clear_moves()
{
  moves_trans.clear();
  moves_create.clear();
  occ_sites.clear();
  moves_create.resize(ntypes+1);
  occ_sites.resize(ntypes+1);
  for (int typ=1; typ<=ntypes; typ++) {
    moves_create[typ].clear();
    occ_sites[typ].clear();
  }
}

void ListofMoves::build_move_list()
{
  clear_moves();
  for (siteit.moveto(0,0,0); siteit!=iconf.lat.end(); ++siteit) {
    if (siteit->type==0)
      add_moves_with_dest(siteit);
    else if (GrandCanonical)
      occ_sites[siteit->type].push_back(&(*siteit));
  }
}

void ListofMoves::
add_moves_with_dest(const typename BMSCConfiguration::GraphT::node_iterator &site)
{
  class allow_new_n : public UnaryFunction<SiteT> {
  public:
    bool ok;
    void operator()(SiteT& s) {ok = ok && (s.type==0 || s.nneighb<s.type);}
  } ANN;


  if (!OnlyGrandCanonical) {
    // add possible translations to here;
    for (int nn=0; nn<6; nn++) {  // check all neighbs as possible sources
      SiteT *src=&(site.neighbour(nn));
      // neighb occupied?
      if (src->type==0) continue;   // nope
      // can this particle move to here?
      if (site->nneighb-1>src->type) continue;  // nope
      // are the other neighbours ok with a part here?
      short srct=src->type;
      src->type=0;    // temporarily remove particle
      ANN.ok=true;
      for_each_neighbour<SiteT>(site,ANN);
      if (ANN.ok)  // yes! add move
	moves_trans.push_back(movedesc_trans(src,&(*site)));
      src->type=srct;  // put it back
    }
  }
    
  if (!GrandCanonical) return;
  // add possible creations here
  ANN.ok=true;
  for_each_neighbour<SiteT>(site,ANN);
  if (ANN.ok) // neighbours allow one more part here
    add_creations_at(site);
}

void ListofMoves::
add_creations_at(const typename BMSCConfiguration::GraphT::node_iterator &site)
{
  // assuming neighbours allow it, add all possible creations
  for (int typ=1; typ<=ntypes; typ++)
    if (site->nneighb<=typ)
      moves_create[typ].push_back(movedesc_create(&(*site)));
}

void ListofMoves::do_move_trans(int m)
{
  (moves_trans[m].to)->type=(moves_trans[m].from)->type;
  (moves_trans[m].from)->type=0;
  
  // substract a neighbour to all neighbours of source
  siteit.moveto(moves_trans[m].from);
  for_each_neighbour<SiteT>(siteit,subs_neigh);
  // add a neighbour to all neighbours of dest
  siteit.moveto(moves_trans[m].to);
  for_each_neighbour<SiteT>(siteit,add_neigh);

  build_move_list();
}

void ListofMoves::do_move_create(int t,int m)
{
  siteit.moveto(moves_create[t][m].dest);
  // add particle at site
  siteit->type=t;
  // add a neighbour to all neighbours of dest
  for_each_neighbour<SiteT>(siteit,add_neigh);

  build_move_list();
}


void ListofMoves::do_move_destroy(int type,int m)
{
  siteit.moveto(occ_sites[type][m]);
  siteit->type=0;
  // substract a neighbour to all neighbours cleared site
  for_each_neighbour<SiteT>(siteit,subs_neigh);

  build_move_list();
}



/******************************************************************************
 *
 * Simulation
 *
 */

class BMSC_standard_observer;

class BMSC_KineticSimulation : public Simulation {
public:
  BMSC_KineticSimulation(BMSCEnvironment&,BMSCConfiguration&);

  const char *name();
  void init_sim(run_mode mode);
  void step();
  void log_start_run(),log_stop_run();
  void log();

  friend class BMSC_standard_observer;

  BMSCEnvironment   &ienv;
  BMSCConfiguration &iconf;

  bool check_config();

protected:
  int    nparts,npart[5];

private:
  long trymov,accmov,trycrdes,acccrdes;
  double alphadot_micro;

  ListofMoves PossibleMoves;

  void count_parts();
} ;

BMSC_KineticSimulation::BMSC_KineticSimulation(BMSCEnvironment &env,
					       BMSCConfiguration &conf) :
  Simulation(env,conf),
  ienv(dynamic_cast<BMSCEnvironment&>(env)),
  iconf(dynamic_cast<BMSCConfiguration&>(conf)),
  PossibleMoves(ienv.GrandCanonical,ienv.OnlyGrandCanonical,conf,ienv.ntypes)
{
}

const char* BMSC_KineticSimulation::name() {return "KineticMC of Biroli-Mezard model";}

void BMSC_KineticSimulation::init_sim(run_mode mode)
{
  count_parts();
  alphadot_micro=ienv.alphadot/iconf.lat.nNodes();
  PossibleMoves.build_move_list();
}

void BMSC_KineticSimulation::count_parts()
{
  nparts=npart[1]=npart[2]=npart[3]=npart[4]=0;
  for (BMSCConfiguration::GraphT::node_literator s=iconf.lat.begin();
       s!=iconf.lat.end(); ++s)
    npart[s->type]++;
  nparts=npart[1]+npart[2]+npart[3]+npart[4];
}

/******************************************************************************
 *
 * Kinetic MC step (grand canonical)
 *
 * WARNING: This function is only good for graphs with constant number of neighbours
 * and alpha>0
 *
 */

void BMSC_KineticSimulation::step()
{
  int    N=iconf.vol;
  int    Ntyp=ienv.ntypes;
  int    Nc=iconf.lat.coordination_number();
  int    t;


  // for (t=1; t<=Ntyp; t++)
  //   ienv.alpha[t] = ienv.alphadot== 0 ? ienv.alpha0[t] :
  //     ienv.alpha0[t]*exp(ienv.alphadot*iconf.time);
  for (t=1; t<=Ntyp; t++)
    ienv.alpha[t] = ienv.alphadot== 0 ? ienv.alpha0[t] :
      ienv.alpha0[t] + ienv.alphadot*iconf.time;



  if (PossibleMoves.Ncreate(1)>0 && PossibleMoves.Ncreate(3)>0) 
    ienv.alpha[3] = ienv.alpha[1] + ::log(7./3.) +
      ::log( (double) PossibleMoves.Ncreate(1) / (double) PossibleMoves.Ncreate(3) );
  else if (PossibleMoves.Ncreate(1)==0)
    if ((double) npart[1]/nparts < 0.3) ienv.alpha[3]*=0.95;








  double Wtrans=(double) 1/(2*N*Nc);
  double Pt=PossibleMoves.Ntrans()*Wtrans;

  double Wcreate[5];
  double Pc[5];
  Pc[0]=Pt;
  for (t=1; t<=Ntyp; t++) {
    Wcreate[t]= (ienv.alpha[t] > 0 ) ? (double) 1/(2*N*Ntyp) :
      exp(ienv.alpha[t])/(2*N*Ntyp);
    Pc[t]=Pc[t-1] + (double) PossibleMoves.Ncreate(t)*Wcreate[t];
  }

  double Wdestroy[5];
  double Pd[5];
  Pd[0]=Pc[Ntyp];
  for (t=1; t<=Ntyp; t++) {
    Wdestroy[t]= (ienv.alpha[t] > 0) ? exp(-ienv.alpha[t])/(2*N*Ntyp) :
      (double) 1/(2*N*Ntyp);
    Pd[t]=Pd[t-1] + (double) PossibleMoves.Ndestroy(t)*Wdestroy[t];
  }
  double Ptot=Pd[Ntyp];

#ifdef DEBUG
    std::cerr << "(DD) Alpha[1]= " << ienv.alpha[1] << ", exp(-alpha)= " << exp(-ienv.alpha[1]) << '\n';
    std::cerr << "(DD) Wtrans= " << Wtrans << ", Wcreate= " << Wcreate << ", Wdestroy[1]= " <<
      Wdestroy[1] << '\n';
    std::cerr << "(DD) Moves found: trans " << PossibleMoves.Ntrans() <<
      " create (1,2,3) " << 
      PossibleMoves.Ncreate(1) << ", " << PossibleMoves.Ncreate(2) << ", " <<
      PossibleMoves.Ncreate(3) << '\n' <<
      "\n      destroy [1] " << PossibleMoves.Ndestroy(1) <<
      " destroy [2] " << PossibleMoves.Ndestroy(2) <<
      " (with " << nparts << " particles)\n";
    std::cerr << "(DD) Pt= " << Pt << ", Pc= " << Pc << ", Ptot= " << Ptot << '\n';
    std::cerr << "(DD) Pd[1]= " << Pd[1] << ", Pd[2]= " << Pd[2] << '\n';
    {int tdest=0;
      for (t=1; t<=Ntyp; t++) tdest+=PossibleMoves.Ndestroy(t);
      if (tdest!=nparts) exit(2);
    }
    // if (!test_moves()) {
    //   std::cerr << "(DD) Move test failed\n";
    //   exit(2);
    // }
    // int nt=PossibleMoves.Ntrans();
    // int nc=PossibleMoves.Ncreate();
    // int nd=PossibleMoves.Ndestroy();
    // PossibleMoves.build_move_list();
    // if (nt!=PossibleMoves.Ntrans() || nc!=PossibleMoves.Ncreate() ||
    // 	nd!=PossibleMoves.Ndestroy() ) {
    //   std::cerr << "(DD) Move test failed: nmoves " << nt << ' ' << nc << ' ' << nd << '\n';
    //   std::cerr << "     da capo         : nmoves " << PossibleMoves.Ntrans() << ' ' <<
    // 	PossibleMoves.Ncreate() << ' ' << PossibleMoves.Ndestroy() << '\n';
    //   exit(2);
    // }
    // std::cerr << "(DD) Moves tested\n";
#endif

    if (Ptot==0) {
      std::cout << "System jammed\n";
      raise(2);
      return;
    }

    double r=Ptot*ienv.ran();
    int   m,move;
    if (r<Pt) {
      move=0;
      m=floor(r/Wtrans);
      PossibleMoves.do_move_trans(m);
    } else if (r<Pc[Ntyp]) {
      move=1;
      t=1;
      while (r>=Pc[t]) t++;
      m=floor((r-Pc[t-1])/Wcreate[t]);
      PossibleMoves.do_move_create(t,m);
      ++npart[t];
      ++nparts;
    } else {
      t=1;
      while (r>=Pd[t]) t++;
      move=2;
      m=floor((r-Pd[t-1])/Wdestroy[t]);
      PossibleMoves.do_move_destroy(t,m);
      --npart[t];
      --nparts;
    }
    iconf.time+=1./(N*Ptot);  // 1/N is the time unit
    ienv.total_time=iconf.time;

#ifdef DEBUG
    switch (move) {
    case 0:
      std::cerr << "(DD) --- r= " << r << ", applied trans move " << m << '\n';
      break;
    case 1:
      std::cerr << "(DD) --- r= " << r << ", applied create move " << m << '\n';
      break;
    case 2:
      std::cerr << "(DD) --- r= " << r << ", applied destroy move " << m << '\n';
      break;
    }
    if (!check_config()) exit(1);
#endif

    // Finalizacion (engañando a run())
    if (iconf.time>=ienv.final_time) ienv.steps_remaining=1;
    else ienv.steps_remaining=10;
}



bool BMSC_KineticSimulation::check_config()
{
  bool ok=true;

  class count_neighbours : public UnaryFunction<SiteT> {
  public:
    count_neighbours() : NN(0) {}
    void operator()(SiteT& s) {if (s.type>0) NN++;}
    int NN;
  } CN;

  int npart_check=0;
  for (BMSCConfiguration::GraphT::node_iterator s(iconf.lat);
       s!=iconf.lat.end(); s++) {
    CN.NN=0;
    for_each_neighbour(s,CN);
    if (s->nneighb!=CN.NN) {
      ok=false;
      std::cout << "ERROR: node " << s.node_id() << " stores wrong number of neighbours (" << s->nneighb << ", correct " << CN.NN << ")\n";
    }
    if (s->type>0 && CN.NN>s->type) {
      ok=false;
      std::cout << "ERROR: node " << s.node_id() << " has too many neighbours (type " << s->type << ", neighbours " << CN.NN << "/" << s->nneighb << ")\n";
    }
    if (s->type>0) npart_check++;
  }
  if (nparts!=npart_check) {
    ok=false;
    std::cout << "ERROR: bad particle count (" << nparts << ", correct " << npart_check << ")\n";
  }
  return ok;
}

void BMSC_KineticSimulation::log_start_run()
{
  std::cout << "Starting at time " << iconf.time << '\n';
  if (check_config()) std::cout << "Configuration at time " << iconf.time << " checked OK\n";
  else exit(1);
  printf("    Time Nparts Density   alpha1   alpha2   alpha3\n");
}

void BMSC_KineticSimulation::log()
{
  printf("%8.4g %6d %7.4f %8.3f %8.3e %8.3e\n",iconf.time,nparts,(double)nparts/iconf.vol,
	 ienv.alpha[1],ienv.alpha[2],ienv.alpha[3]);
}

void BMSC_KineticSimulation::log_stop_run()
{
  if (check_config()) std::cout << "Configuration at time " << iconf.time << " checked OK\n";
}

//////////////////////////////////////////////////////////////////////////////
//
// Observable

class BMSC_standard_observer : public Observable {
private:
  BMSC_KineticSimulation &sim;
  BMSCEnvironment        &ienv;
  BMSCConfiguration      &iconf;
  FILE                   *of;

  void start(Simulation::run_mode);
  void do_observation();

public:
  BMSC_standard_observer(Simulation &sim);
  ~BMSC_standard_observer() {fclose(of);}
} ;

BMSC_standard_observer::BMSC_standard_observer(Simulation &s) :
  Observable("Pctcc standard observables",
	     dynamic_cast<BMSC_KineticSimulation&>(s).ienv,
	     dynamic_cast<BMSC_KineticSimulation&>(s).iconf,
	     dynamic_cast<BMSC_KineticSimulation&>(s).ienv.obs_step),
  sim(dynamic_cast<BMSC_KineticSimulation&>(s)),
  iconf(sim.iconf),
  ienv(sim.ienv)
{
}

void BMSC_standard_observer::start(Simulation::run_mode rmode)
{
  switch (rmode) {
  case Simulation::new_run:
    of=fopen(sim.env.default_observation_file.c_str(),"w");
    assert(of);
    fprintf(of,"# Pctcc standard observer, step: %d\n",step);
    fprintf(of,"#       Time   alpha1   alpha2   alpha3  Density N1/Ntot N2/Ntot N3/Ntot N4/Ntot\n");
    break;
  case Simulation::continuation_run:
    of=fopen(sim.env.default_observation_file.c_str(),"a");
    assert(of);
    break;
  }
  Observable::start(rmode);
}

void BMSC_standard_observer::do_observation()
{
  double dens=(double)sim.nparts/iconf.vol;
  double c1=(double)sim.npart[1]/sim.nparts;
  double c2=(double)sim.npart[2]/sim.nparts;
  double c3=(double)sim.npart[3]/sim.nparts;
  double c4=(double)sim.npart[4]/sim.nparts;

  fprintf(of,"%12.8g %8.4g %8.4g %8.4g %8.4f %7.4f %7.4f %7.4f %7.4f %7.4f\n",iconf.time,
	  ienv.alpha[1],ienv.alpha[2],ienv.alpha[3],dens,c1,c2,c3,c4);
}

#endif /* _BM_KINETIC_HH_ */
