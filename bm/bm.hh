/* 
 * bm.hh -- Standard MC for the Biroli-Mezard model
 *
 * This contains the simulation class for single-spin-flip Metropolis
 * Monte Carlo of the lattice glass invented by Biroli and Mezard, on
 * the simple cubic lattice.
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef BM_HH
#define BM_HH

#include <math.h>

#include "glsim/mc_parameters.hh"
#include "glsim/mc_environment.hh"
#include "glsim/simulation.hh"
#include "glsim/observable.hh"
#include "../graph/lattice3D.hh"

/******************************************************************************
 *
 * Parameters
 *
 */

class BMSCParameters : public MonteCarlo_parameters {
public:
  BMSCParameters();
} ;

BMSCParameters::BMSCParameters() :
  MonteCarlo_parameters()
{
  ctrl_file_options.add_options()
    ("BM.GrandCanonical",po::value<bool>()->default_value(false),"Allow grand canonical moves")
    ("BM.ntypes",po::value<int>()->default_value(1),"")
    ("BM.Lx",po::value<int>()->default_value(10),"")
    ("BM.Ly",po::value<int>()->default_value(10),"")
    ("BM.Lz",po::value<int>()->default_value(10),"")
    ("BM.alphadot",po::value<double>()->default_value(0.),"rate of change of alpha")
    ("BM.alpha1",po::value<double>()->default_value(1.),"mu for type 1")
    ("BM.alpha2",po::value<double>()->default_value(1.),"mu for type 2")
    ("BM.alpha3",po::value<double>()->default_value(1.),"mu for type 3")
    ("BM.alpha4",po::value<double>()->default_value(1.),"mu for type 4")
    ;
}

/******************************************************************************
 *
 * Configuration
 *
 */

struct SiteT {
  unsigned  nneighb : 4;
  unsigned  type    : 4;
} ;

class BMSCConfiguration : public Configuration {
public:
  typedef PeriodicSCLattice<SiteT> GraphT;

  GraphT  lat;
  int     vol;
  double  time;

  BMSCConfiguration(int Lx,int Ly,int Lz);
  void deflt(const char*);
  void load(const char*);
  void save(const char*);
} ;

BMSCConfiguration::BMSCConfiguration(int Lx,int Ly,int Lz) :
  Configuration("BM on the SC lattice"),
  lat(Lx,Ly,Lz)
{
  vol=lat.nNodes();
  deflt(0);
}

void BMSCConfiguration::deflt(const char* fname)
{
  if (fname!=0) 
    load(fname);
  else {
    for (GraphT::node_literator s=lat.begin(); s!=lat.end(); s++)
      s->nneighb=s->type=0;
  }
  vol=lat.nNodes();
  time=0;
}

void BMSCConfiguration::save(const char* file)
{
  std::ofstream f(file,std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);
  f.write((char*)&time,sizeof(time));
  lat.write(f);
}

void BMSCConfiguration::load(const char* file)
{
  std::ifstream f(file,std::ios_base::in | std::ios_base::binary);
  f.read((char*)&time,sizeof(time));
  lat.read(f);
  vol=lat.nNodes();
}

/******************************************************************************
 *
 * Environment
 *
 */

class BMSCEnvironment : public MonteCarlo_environment {
public:
  bool   GrandCanonical;
  int    Lx,Ly,Lz;
  int    ntypes;
  double T,alphadot,alpha[5];

  BMSCEnvironment(const char* efile_read="environment_ini.dat",
		   const char* efile_write="environment_fin.dat",
		   const char* cfile_read="configuration_ini.dat",
		   const char* cfile_write="configuration_fin.dat");
  BMSCEnvironment(int argc,char *argv[],BMSCParameters &par);

private:
  void register_vars();
} ;

BMSCEnvironment::BMSCEnvironment(int argc,char *argv[],BMSCParameters &par) :
  MonteCarlo_environment(argc,argv,par)
{
  GrandCanonical=par.value("BM.GrandCanonical").as<bool>();
  Lx=par.value("BM.Lx").as<int>();
  Ly=par.value("BM.Ly").as<int>();
  Lz=par.value("BM.Lz").as<int>();
  T=temperature;
  alphadot=par.value("BM.alphadot").as<double>();
  ntypes=par.value("BM.ntypes").as<int>();
  alpha[1]=par.value("BM.alpha1").as<double>();
  alpha[2]=par.value("BM.alpha2").as<double>();
  alpha[3]=par.value("BM.alpha3").as<double>();
  alpha[4]=par.value("BM.alpha4").as<double>();
  register_vars();
}

BMSCEnvironment::BMSCEnvironment(const char* efr,const char* efw,const char* cfr,
				 const char* cfw) :
  MonteCarlo_environment(efr,efw,cfr,cfw),
  Lx(10),
  Ly(10),
  Lz(10),
  ntypes(1)
{
  alpha[1]=1;
  register_vars();
}

void BMSCEnvironment::register_vars()
{
  persist.reg_var("BMSCEnvironment.Lx",&Lx,pa::write_only);
  persist.reg_var("BMSCEnvironment.Ly",&Ly,pa::write_only);
  persist.reg_var("BMSCEnvironment.Lz",&Lz,pa::write_only);
  persist.reg_var("BMSCEnvironment.T",&T,pa::write_only);
  persist.reg_var("BMSCEnvironment.alphadot",&alphadot,pa::write_only);
  persist.reg_var("BMSCEnvironment.ntypes",&ntypes,pa::write_only);
  persist.reg_var("BMSCEnvironment.alpha1",alpha+1,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha2",alpha+2,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha3",alpha+3,pa::read_write);
  persist.reg_var("BMSCEnvironment.alpha4",alpha+4,pa::read_write);
}


/******************************************************************************
 *
 * Simulation
 *
 */

class BMSC_standard_observer;

class BMSCSimulation : public Simulation {
public:
  BMSCSimulation(BMSCEnvironment&,BMSCConfiguration&);

  const char *name();
  void init_sim(run_mode mode);
  void step();
  void log_start_run(),log_stop_run();
  void log();

  friend class BMSC_standard_observer;

  BMSCEnvironment   &ienv;
  BMSCConfiguration &iconf;

  bool check_config();

protected:
  int    nparts,npart[5];

private:
  long trymov,accmov,trycrdes,acccrdes;
  double alphadot_micro;

  void count_parts();
} ;

BMSCSimulation::BMSCSimulation(BMSCEnvironment &env,BMSCConfiguration &conf) :
  Simulation(env,conf),
  ienv(dynamic_cast<BMSCEnvironment&>(env)),
  iconf(dynamic_cast<BMSCConfiguration&>(conf)),
  trymov(0),
  accmov(0),
  trycrdes(0),
  acccrdes(0)
{
}

const char* BMSCSimulation::name() {return "Biroli-Mezard model";}

void BMSCSimulation::init_sim(run_mode mode)
{
  count_parts();
  alphadot_micro=ienv.alphadot/iconf.lat.nNodes();
}

void BMSCSimulation::count_parts()
{
  nparts=npart[1]=npart[2]=npart[3]=npart[4]=0;
  for (BMSCConfiguration::GraphT::node_literator s=iconf.lat.begin();
       s!=iconf.lat.end(); ++s)
    npart[s->type]++;
  nparts=npart[1]+npart[2]+npart[3]+npart[4];
}

void BMSCSimulation::step()
{
  int    typ;

  typename BMSCConfiguration::GraphT& lat=iconf.lat;
  static typename BMSCConfiguration::GraphT::node_iterator site(lat),site0(lat);
  typename BMSCConfiguration::GraphT::node_literator fsite;

  class substract_n : public UnaryFunction<SiteT> {
    void operator()(SiteT& s) {s.nneighb--;}
  } subs_neigh;
  class add_n : public UnaryFunction<SiteT> {
    void operator()(SiteT& s) {s.nneighb++;}
  } add_neigh;
  class allow_new_n : public UnaryFunction<SiteT> {
  public:
    bool ok;
    void operator()(SiteT& s) {ok = ok && (s.type==0 || s.nneighb<s.type);}
  } ANN;

  for (int itry=0; itry<lat.nNodes(); itry++) {

    // Motion
    site.random();
    if (site->type==0) goto grand_canonical; // site empty
    trymov++;
    site0=site;
    site.random_neighbour();
    if (site->type!=0) goto grand_canonical; // site occupied; move failed
    if (site->nneighb-1>site0->type) goto grand_canonical; // too many neghbours, fail
    // check if neighbours of new site allow more neighbours
    typ=site0->type;
    site0->type=0;
    ANN.ok=true;
    for_each_neighbour<SiteT>(site,ANN);
    if (!ANN.ok) {site0->type=typ; continue;} // neighbours do not allow more neighbours
    // success
    site->type=typ;
    for_each_neighbour<SiteT>(site,add_neigh);
    for_each_neighbour<SiteT>(site0,subs_neigh);
    accmov++;

    // Creation/destruction
  grand_canonical:
    if (!ienv.GrandCanonical) continue;
    trycrdes++;
    site.random();
    if (site->type==0) {  // Create particle
      ANN.ok=true;
      for_each_neighbour<SiteT>(site,ANN);
      if (!ANN.ok) continue; // neighbours do not allow more neighbours
      typ=ienv.ran[ienv.ntypes]+1;
      if (site->nneighb>typ) continue;  // too many neighbours
      // compute exp(alpha*(Nnew-Nold))=exp(alpha)
      if (ienv.alpha[typ]>=0 || ienv.ran()<exp(ienv.alpha[typ])) { // accept
	site->type=typ;
	nparts++;
	npart[typ]++;
	acccrdes++;
	for_each_neighbour(site,add_neigh);
      }
    } else {
      if (ienv.ran()<1./ienv.ntypes)  	// add prob for detailed balance
	// compute exp(alpha*(Nnew-Nold))=exp(-alpha)
	if (ienv.alpha[site->type]<0 || ienv.ran()<exp(-ienv.alpha[site->type]) ) {
	  nparts--;
	  npart[site->type]--;
	  site->type=0;
	  acccrdes++;
	  for_each_neighbour(site,subs_neigh);
	}
    }

    ienv.alpha[1]+=alphadot_micro;
    ienv.alpha[2]+=alphadot_micro;
    ienv.alpha[3]+=alphadot_micro;
    ienv.alpha[4]+=alphadot_micro;

  }
  iconf.time+=1.;
}

bool BMSCSimulation::check_config()
{
  bool ok=true;

  class count_neighbours : public UnaryFunction<SiteT> {
  public:
    count_neighbours() : NN(0) {}
    void operator()(SiteT& s) {if (s.type>0) NN++;}
    int NN;
  } CN;

  int npart_check=0;
  for (BMSCConfiguration::GraphT::node_iterator s(iconf.lat);
       s!=iconf.lat.end(); s++) {
    CN.NN=0;
    for_each_neighbour(s,CN);
    if (s->nneighb!=CN.NN) {
      ok=false;
      std::cout << "ERROR: node " << s.node_id() << " stores wrong number of neighbours (" << s->nneighb << ", correct " << CN.NN << ")\n";
    }
    if (s->type>0 && CN.NN>s->type) {
      ok=false;
      std::cout << "ERROR: node " << s.node_id() << " has too many neighbours (type " << s->type << ", neighbours " << CN.NN << "/" << s->nneighb << ")\n";
    }
    if (s->type>0) npart_check++;
  }
  if (nparts!=npart_check) {
    ok=false;
    std::cout << "ERROR: bad particle count (" << nparts << ", correct " << npart_check << ")\n";
  }
  return ok;
}


void BMSCSimulation::log_start_run()
{
  std::cout << "Starting at time " << iconf.time << '\n';
  if (check_config()) std::cout << "Configuration at time " << iconf.time << " checked OK\n";
  else exit(1);
  printf("    Time Nparts Density alpha1   accmov  acccrdes\n");
}

void BMSCSimulation::log()
{
  printf("%8.4g %6d %7.4f %6.3f %8.3e %8.3e\n",iconf.time,nparts,(double)nparts/iconf.vol,
	 ienv.alpha[1],(double)accmov/trymov,(double)acccrdes/trycrdes);
}

void BMSCSimulation::log_stop_run()
{
  if (check_config()) std::cout << "Configuration at time " << iconf.time << " checked OK\n";
}

//////////////////////////////////////////////////////////////////////////////
//
// Observable

class BMSC_standard_observer : public Observable {
private:
  BMSCSimulation     &sim;
  BMSCEnvironment    &ienv;
  BMSCConfiguration  &iconf;
  FILE               *of;

  void start(Simulation::run_mode);
  void do_observation();

public:
  BMSC_standard_observer(Simulation &sim);
  ~BMSC_standard_observer() {fclose(of);}
} ;

BMSC_standard_observer::BMSC_standard_observer(Simulation &s) :
  Observable("Pctcc standard observables",
	     dynamic_cast<BMSCSimulation&>(s).ienv,
	     dynamic_cast<BMSCSimulation&>(s).iconf,
	     dynamic_cast<BMSCSimulation&>(s).ienv.obs_step),
  sim(dynamic_cast<BMSCSimulation&>(s)),
  iconf(sim.iconf),
  ienv(sim.ienv)
{
}

void BMSC_standard_observer::start(Simulation::run_mode rmode)
{
  switch (rmode) {
  case Simulation::new_run:
    of=fopen(sim.env.default_observation_file.c_str(),"w");
    assert(of);
    fprintf(of,"# Pctcc standard observer, step: %d\n",step);
    fprintf(of,"#       Time   alpha1 Nparts Density N1/Ntot N2/Ntot N3/Ntot N4/Ntot\n");
    break;
  case Simulation::continuation_run:
    of=fopen(sim.env.default_observation_file.c_str(),"a");
    assert(of);
    break;
  }
  Observable::start(rmode);
}

void BMSC_standard_observer::do_observation()
{
  double dens=(double)sim.nparts/iconf.vol;
  double c1=(double)sim.npart[1]/sim.nparts;
  double c2=(double)sim.npart[2]/sim.nparts;
  double c3=(double)sim.npart[3]/sim.nparts;
  double c4=(double)sim.npart[4]/sim.nparts;

  fprintf(of,"%12.8g %8.4f %6d %7.4f %7.4f %7.4f %7.4f %7.4f\n",iconf.time,ienv.alpha[1],sim.nparts,dens,
	  c1,c2,c3,c4);
}


#endif /* BM_HH */
