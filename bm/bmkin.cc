/* 
 * bmkin.cc -- Kinetic  MC for the BM model on the simple cubic
 *             lattice
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#include "bm-kinetic.hh"

typedef BMSCParameters SimulationParameters;
typedef BMSCEnvironment SimulationEnvironment;

Parameters *create_parameters()
{
  return new SimulationParameters();
}

Environment *create_environment(int argc,char *argv[],Parameters *par)
{
  return new SimulationEnvironment(argc,argv,*dynamic_cast<SimulationParameters*>(par));
}

Configuration *create_configuration(Environment *env)
{
  SimulationEnvironment *lpar=dynamic_cast<SimulationEnvironment*>(env);

  return new BMSCConfiguration(lpar->Lx,lpar->Ly,lpar->Lz);
}

Simulation *create_simulation(Environment *e,Configuration *c)
{
  SimulationEnvironment& env=dynamic_cast<SimulationEnvironment&>(*e);
  BMSC_KineticSimulation *sim=
    new BMSC_KineticSimulation(dynamic_cast<SimulationEnvironment&>(*e),dynamic_cast<BMSCConfiguration&>(*c));
  if (e->obs_step>0)
    sim->add_obs(new BMSC_standard_observer(*sim));
  // if (env.ov_step>0) sim->add_obs(new OverlapSC<SimulationEnvironment>(*sim));
  return sim;
}
