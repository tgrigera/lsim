/* 
 * isingenv.hh - Classes for Monte Carlo of the Ising model on a generic graph
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 * The energy of the ferromagnetic Ising model is
 *
 *  E(\{S_\i}) = -J \sum_{\langle ij \rangle} S_i S_j - h \sum_i S_i
 *
 * where $S_i=\pm1$ are ``spin'' variables associated with the nodes
 * of a graph, and the sum is over all the nodes joined by a link. The
 * problem is completely defined only when the graph is given. Here we
 * present a class for the Metropolis Monte Carlo simulation of the
 * Ising model on a general graph. We shall assume J=1.
 * 
 */

#ifndef ISINGENV_HH
#define ISINGENV_HH

#include "glsim/stochastic.hh"

/******************************************************************************
 *
 * Base environment and parameters (this must be complemented by a class
 * that reads the lattice parameters, see e.g. isingSC.hh)
 *
 */

class Ising_parameters_base : public glsim::Parameters {
public:
  Ising_parameters_base(const char *scope);
} ;

class Ising_environment_base : public glsim::SimEnvironment {
public:
  bool   default_config_ordered;
  long   MCsteps;
  double field;
  double temperature,beta,betah;
  double energy;
  long   mag;

  glsim::StochasticEnvironment SE;

  Ising_environment_base(const char* scope=glsim::Parameters::default_scope);

protected:
  void  init_local(),warm_init_local();

private:
  Ising_parameters_base  par;

  void vserial(oarchive_t &ar) {ar << *this;}
  void vserial(iarchive_t &ar) {ar >> *this;}
  template <typename Archive>
  void serialize(Archive &ar,const unsigned int version);
  friend class boost::serialization::access;

public:
  static const int class_version=1;
} ;

BOOST_CLASS_VERSION(Ising_environment_base,Ising_environment_base::class_version);

#endif /* ISINGENV_HH */
