/* 
 * isingtraj.hh -- Write offlattice-trajectory-like files
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 * #include this in isingXX.cc *after* you have declared the actual
 * configuration (with the lattice type defined)
 *
 */

#ifndef ISINGTRAJ_HH
#define ISINGTRAJ_HH

#include "glsim/parameters.hh"
#include "glsim/environment.hh"
#include "glsim/olconfiguration.hh"

class Ising_trajectory_parameters : public glsim::Parameters {
public:
  Ising_trajectory_parameters(const char* scope=Parameters::default_scope) :
    Parameters(scope)
  {parameter_file_options().add_options()
      ("trajectory.obs_interval",po::value<int>()->default_value(0),
       "observation interval, 0=don't observe")
      ("trajectory.file_prefix",po::value<std::string>(),
       "file prefix for trajectories")
      ("trajectory.lattice_parameter",po::value<double>()->default_value(1.),
       "lattice spacing")
      ;
  }
} ;

template <typename Ising_conf>
class Ising_trajectory : public glsim::Environment {
public:
  Ising_trajectory(const glsim::SimEnvironment&,const Ising_conf&);
  virtual ~Ising_trajectory();
  void observe_first();
  void step_local();

protected:
  void init_local();
  void warm_init_local();

  glsim::OLconfig_file *of;
  std::string   traj_file_prefix,traj_fname;
  int           obs_interval;  

private:
  bool                   first_observed;
  const Ising_conf       &conf;
  glsim::OLconfiguration *iconf;
  glsim::OLconfig_file::options file_opt;
  const glsim::SimEnvironment&  senv;
  double                 lattspace;
  Ising_trajectory_parameters  par;

  void                  common_init();
  void                  store_positions(glsim::OLconfiguration*,const Ising_conf&);
  friend class boost::serialization::access;
  template <typename Archive>
  void serialize(Archive &ar,const unsigned int version);
  virtual void vserial(iarchive_t &ar) {ar >> *this;}
  virtual void vserial(oarchive_t &ar) {ar << *this;}
} ;

template <typename Ising_conf> inline Ising_trajectory<Ising_conf>::
Ising_trajectory(const glsim::SimEnvironment &e,const Ising_conf& c) :
  Environment(e.scope()),
  of(0),
  traj_file_prefix("traj"),
  traj_fname("[AUTO]"),
  obs_interval(0),
  first_observed(false),
  conf(c),
  iconf(0),
  senv(e),
  par(e.scope())
{}

template <typename Ising_conf> inline Ising_trajectory<Ising_conf>::
Ising_trajectory::~Ising_trajectory()
{
  delete of;
  delete iconf;
}

template <typename Ising_conf>
void Ising_trajectory<Ising_conf>::init_local()
{
  glsim::Environment::init_local();
  first_observed=false;
  common_init();
}

template <typename Ising_conf>
void Ising_trajectory<Ising_conf>::warm_init_local()
{
  glsim::Environment::warm_init_local();
  common_init();
}

template <typename Ising_conf>
void Ising_trajectory<Ising_conf>::common_init()
{
  traj_fname="[AUTO]";
  obs_interval=par.value("trajectory.obs_interval").as<int>();
  if (obs_interval>0) {
    traj_file_prefix=par.value("trajectory.file_prefix").as<std::string>();
    final_filename(traj_fname,traj_file_prefix);
    lattspace=par.value("trajectory.lattice_parameter").as<double>();
  }
}

template <typename Ising_conf>
template <typename Arch>
void Ising_trajectory<Ising_conf>::serialize(Arch &ar, const unsigned version)
{
  ar & traj_fname & obs_interval;
  ar & first_observed;
}

/// This *must* be called before the first simulation step (but after
/// simulation construction).
template <typename Ising_conf>
void Ising_trajectory<Ising_conf>::observe_first()
{
  if (obs_interval==0) return;
  if (!of) {
    iconf=new glsim::OLconfiguration();
    iconf->N=conf.vol;
    file_opt.time_frame();
    iconf->box_angles[0]=iconf->box_angles[1]=iconf->box_angles[2]=90;
    iconf->id=new short[iconf->N];
    for (int i=0; i<iconf->N; ++i) iconf->id[i]=i;
    iconf->r=new double[iconf->N][3];
    store_positions(iconf,conf);
    iconf->v=new double[iconf->N][3];
    memset(iconf->v,0,3*iconf->N*sizeof(double));
    file_opt.v_frame();
    of=new glsim::OLconfig_file(iconf,file_opt);
    of->create(traj_fname.c_str());
    of->write_header();
  }
  if (first_observed) return;
  step_local();
  first_observed=true;
}

template <typename Ising_conf>
void Ising_trajectory<Ising_conf>::step_local()
{
  if (obs_interval==0) return;
  if (senv.steps_completed % obs_interval==0) {
    iconf->step=conf.step;
    iconf->time=iconf->step;
    int i=0;
    for (auto site=conf.lat->begin(); site!=conf.lat->end(); ++site) {
      iconf->v[i][0]=*site;
      ++i;
    }
    of->append_record();
  }
}

#endif /* ISINGTRAJ_HH */
