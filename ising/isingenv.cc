/* 
 * isingenv.cc - Classes for Monte Carlo of the Ising model on a generic graph
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 * The energy of the ferromagnetic Ising model is
 *
 *  E(\{S_\i}) = -J \sum_{\langle ij \rangle} S_i S_j - h \sum_i S_i
 *
 * where $S_i=\pm1$ are ``spin'' variables associated with the nodes
 * of a graph, and the sum is over all the nodes joined by a link. The
 * problem is completely defined only when the graph is given. Here we
 * present a class for the Metropolis Monte Carlo simulation of the
 * Ising model on a general graph. We shall assume J=1.
 * 
 */

#include "isingenv.hh"

/******************************************************************************
 *
 * Base environment and parameters
 *
 */

Ising_parameters_base::Ising_parameters_base(const char *scope) :
  Parameters(scope)
{
  parameter_file_options().add_options()
    ("ising.steps",po::value<long>()->default_value(10),"steps to run")
    ("ising.temperature",po::value<double>()->default_value(0.),"temperature")
    ("ising.field",po::value<double>()->default_value(0.),"magnetic field")
    ("ising.default_configuration_ordered",po::value<bool>()->default_value(false),
     "if true, the default configuration will be generated with magnetization 1")
    ;
}

Ising_environment_base::Ising_environment_base(const char* scope) :
  SimEnvironment(scope),
  MCsteps(10),temperature(1),field(0.),beta(1),betah(1),
  energy(0.),mag(0),
  par(scope)
{}

void Ising_environment_base::init_local()
{
  SimEnvironment::init_local();
  MCsteps=par.value("ising.steps").as<long>();
  temperature=par.value("ising.temperature").as<double>();
  field=par.value("ising.field").as<double>();
  default_config_ordered=par.value("ising.default_configuration_ordered").as<bool>();
  beta=1./temperature;
  betah=beta*field;
}

void Ising_environment_base::warm_init_local()
{
  SimEnvironment::warm_init_local();
  MCsteps=par.value("ising.steps").as<long>();
  field=par.value("ising.field").as<double>();
  temperature=par.value("ising.temperature").as<double>();
  beta=1./temperature;
  betah=beta*field;
}

template <typename Archive>
inline void Ising_environment_base::serialize(Archive &ar,const unsigned int version)
{
  if (version!=class_version)
    throw glsim::Environment_wrong_version("Ising_environment_base",version,class_version);
  ar & boost::serialization::base_object<glsim::SimEnvironment>(*this);
  ar & MCsteps & temperature & beta & betah & field & energy & mag;
}

