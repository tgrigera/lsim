/* 
 * ising.hh - Classes for Monte Carlo of the Ising model on a generic graph
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 * The energy of the ferromagnetic Ising model is
 *
 *  E(\{S_\i}) = -J \sum_{\langle ij \rangle} S_i S_j - h \sum_i S_i
 *
 * where $S_i=\pm1$ are ``spin'' variables associated with the nodes
 * of a graph, and the sum is over all the nodes joined by a link. The
 * problem is completely defined only when the graph is given. Here we
 * present a class for the Metropolis Monte Carlo simulation of the
 * Ising model on a general graph. We shall assume J=1.
 * 
 */

#ifndef ISING_HH
#define ISING_HH

#include <numeric>
#include <math.h>

#include "glsim/simulation.hh"
#include "glsim/observable.hh"
#include "glsim/avevar.hh"
#include "glsim/binvec.hh"

class Ising_simulation : public glsim::Simulation {
public:
  Ising_simulation(Ising_environment&,Ising_conf&);
  ~Ising_simulation();

  Ising_environment   &env;
  Ising_conf          &conf;

  const char *name() const;
  void        log();
  void        log_start_sim();

protected:
  long          energy0;
  int           z;
  unsigned long RANMAX,RANMIN;
  typedef       unsigned long (*probt_t)[2];
  probt_t       probt;
  std::vector<probt_t> probt_vector;
  // unsigned long (*probt)[2];
  glsim::Uniform_integer ran;
  
  void          EnerMag();
} ;

Ising_simulation::Ising_simulation(Ising_environment &env_,Ising_conf &conf_) :
  Simulation(env_,conf_),
  env(env_),
  conf(conf_),
  ran(2)
{
  RANMAX=ran.max();
  RANMIN=ran.min();

  z=conf.lat->coordination_number();
  if (z<0) { // Fluctuating connectivity
    probt=0;
    int zmax=0;
    for (int i=0; i<conf.lat->size(); ++i) {
      int zi=conf.lat->neighbour_size(i);
      if (zi>zmax) zmax=zi;
    }
    probt_vector.resize(zmax+1);
    for (z=0; z<=zmax; ++z) {
      probt_vector[z]=new unsigned long[z+1][2];
      for (int S=0; S<2; S++)
	for (int H=0; H<z+1; H++) {
	  double DE=( 2*(-z+2*H) + 2*env.field ) * (2*S-1);
	  probt_vector[z][H][S] = DE<0  ?  RANMAX :
	    (unsigned long) ((RANMAX-RANMIN)*exp(-env.beta*DE)) + RANMIN;
	}
    }
  } else {
    probt=new unsigned long[z+1][2];
    for (int S=0; S<2; S++)
      for (int H=0; H<z+1; H++) {
	double DE=( 2*(-z+2*H) + 2*env.field ) * (2*S-1);
	probt[H][S] = DE<0  ?  RANMAX :
	  (unsigned long) ((RANMAX-RANMIN)*exp(-env.beta*DE)) + RANMIN;
      }
  }
  
  EnerMag();
}

Ising_simulation::~Ising_simulation()
{
  if (probt) delete[] probt;
  std::for_each(probt_vector.begin(),probt_vector.end(),
		[this](probt_t p){delete[] p;});
}

void Ising_simulation::EnerMag()
{
  double mag=0;
  std::for_each(conf.lat->data(),conf.lat->data()+conf.lat->size(),[&mag](short s){mag+=s;});
  env.mag=mag;
  energy0=0;
  for_each_bond(*(conf.lat),[this](short s1,short s2) {this->energy0-=s1*s2;});
  env.energy=energy0-env.field*env.mag;
}


class Ising_simulation_regular : public Ising_simulation {
public:
  Ising_simulation_regular(Ising_environment& env,Ising_conf& conf) :
    Ising_simulation(env,conf) {}
  void step();
} ;

/**
  This is the simulation step. To compute the change in energy \f$\Delta
  E\f$ when spin $S_i$ is flipped, we write the energy as
  \f[ E = - H_i S_i - h S_i + \mbox{terms not involving $S_i$},
    \qquad H_i = \sum_{j\in
    \mathrm{nn}(i)} S_j,\f]
  so that
  \f[ \Delta E = - (H_i + h) \Delta S_i = 2 H_i S_i + 2 h S_i \equiv
  \Delta E_0 + 2 h S_i, \f] where $S_i$ stands for the value of the spin
  _before_ flipping.  Note that $H_i$ can take only $z+1$ values
  ($z$ is the coordination number), so that if $h$ is fixed, as we
  assume, $\Delta E$ also takes only a few values. If in addition
  $\beta$ is constant for all the run, we can speed up the simulation by
  precomputing all possible values of $e^{-\beta \Delta E}$. Let us
  store in [[prob[H][S]]] the acceptance probabilities for given $H_i$
  and $S_i$.
 
  The following general routine is correct for any regular graph.
  However, it is not efficient for graphs with high connectivity, so
  we shall provide specialized versions below.  For random graphs, use
  Ising_simulation_random.
 
 */

void Ising_simulation_regular::step()
{
  typename Ising_conf::GraphT* lat=conf.lat;
  static typename Ising_conf::GraphT::node_iterator site(*lat);

  for (int tries=0; tries<lat->size(); tries++) {
    site.to(lat->data()+ran(lat->size()));
    int H=0;
    for_each_neighbour(site,[&H](short s){H+=s;});
    double p=probt[(H+z)/2][(*site+1)/2];

    if (p==RANMAX || ran.raw() < p) {
      energy0+=2*H*(*site);
      env.mag+=(-2**site);
      *site*=-1;
      env.energy=energy0-env.field*env.mag;
    }

  }
  conf.step=env.steps_completed;
  env.run_completed = env.steps_in_run>=env.MCsteps;
}

class Ising_simulation_random : public Ising_simulation {
public:
  Ising_simulation_random(Ising_environment& env,Ising_conf& conf) :
    Ising_simulation(env,conf) {}
  void step();
} ;

/**
  This is the simulation step for the random lattice.  It is similar
  to the regular case but attention must be paid to the fluctuating
  connectivity.
*/
void Ising_simulation_random::step()
{
  typename Ising_conf::GraphT* lat=conf.lat;
  static typename Ising_conf::GraphT::node_iterator site(*lat);

  for (int tries=0; tries<lat->size(); tries++) {
    site.to(lat->data()+ran(lat->size()));
    int H=0;
    for_each_neighbour(site,[&H](short s){H+=s;});
    int zloc=site.neighbour_size();
    double p=probt_vector[zloc][(H+zloc)/2][(*site+1)/2];

    if (p==RANMAX || ran.raw() < p) {
      energy0+=2*H*(*site);
      env.mag+=(-2**site);
      *site*=-1;
      env.energy=energy0-env.field*env.mag;
    }

  }
  conf.step=env.steps_completed;
  env.run_completed = env.steps_in_run>=env.MCsteps;
  //  conf.step++;
}


void Ising_simulation::log()
{
  static char buf[200];
  sprintf(buf,"Step %5ld mag %12.5e ener %12.5e\n",
	  conf.step,(double) env.mag/conf.vol,(double) env.energy/conf.vol);
  std::cout << buf;
}

void Ising_simulation::log_start_sim()
{
  std::cout << "Simulation starting\n";
  std::cout << "Number of sites = " << conf.vol << '\n'
	    << "Field           = " << env.field << '\n'
	    << "Temperature     = " << env.temperature
	    << " (beta = " << env.beta << ")\n";
  std::cout << '\n';
  log();
}

/******************************************************************************
 *
 * Observables
 *
 */

class Ising_observable_parameters : public glsim::Parameters {
public:
  Ising_observable_parameters(const char* scope);
} ;

Ising_observable_parameters::Ising_observable_parameters(const char* scope) :
  Parameters(scope)
{
  parameter_file_options().add_options()
    ("ising.obs_interval",po::value<int>()->default_value(0),
     "Interval for standard observation, 0=skip")
    ("ising.obs_file_prefix",po::value<std::string>(),"Observation file prefix")
    ;
}

class Ising_observable : public glsim::SBObservable {
public:
  Ising_observable(Ising_environment&,Ising_conf&);

  void interval_and_file();
  void write_header();
  void observe();

private:
  Ising_environment  &env;
  Ising_conf         &conf;
  Ising_observable_parameters par;

} ;

inline Ising_observable::Ising_observable(Ising_environment& e,Ising_conf &c) :
  SBObservable(e),
  env(e),
  conf(c),
  par(e.scope())
{}

void Ising_observable::interval_and_file()
{
  obs_interval=par.value("ising.obs_interval").as<int>();
  obs_file_prefix=par.value("ising.obs_file_prefix").as<std::string>();
}

void Ising_observable::write_header()
{
  fprintf(of,"# Ising standard observer, interval step: %d\n",obs_interval);
  fprintf(of,"# Step  mag  ener\n");
}

void Ising_observable::observe()
{
  fprintf(of,"%5ld %12.5e %12.5e\n",
	  conf.step,(double) env.mag/conf.vol,(double) env.energy/conf.vol);
  fflush(of);
}  


/******************************************************************************
 *
 * Observable: C(r) and correlation length
 *
 */

class Ising_spacecorr_parameters : public glsim::Parameters {
public:
  Ising_spacecorr_parameters(const char* scope);
} ;

Ising_spacecorr_parameters::Ising_spacecorr_parameters(const char* scope) :
  Parameters(scope)
{
  parameter_file_options().add_options()
    ("ising_spacecorr.obs_interval",po::value<int>()->default_value(0),
     "Interval for C(k) and Cr) computation, 0=skip")
    ("ising_spacecorr.obs_file_prefix",po::value<std::string>(),"Prefix for space correlations file")
    ;
}

class Ising_spacecorr : public glsim::SBObservable {
public:
  Ising_spacecorr(Ising_environment&,Ising_conf&);
  ~Ising_spacecorr();

  void interval_and_file();
  void write_header();
  void observe();

private:
  Ising_environment  &env;
  Ising_conf         &conf;
  Ising_spacecorr_parameters par;

  glsim::AveVar<false> Mag,MagSq;
  std::vector<glsim::AveVar<false>> Ck;
  double deltak;
  glsim::AveVar<false> Cr0;
  glsim::Binned_vector<glsim::AveVar<false>> *Cr;
} ;

inline Ising_spacecorr::Ising_spacecorr(Ising_environment& e,Ising_conf &c) :
  SBObservable(e),
  env(e),
  conf(c),
  par(e.scope()),
  Cr(0)
{
  Ck.resize(10);
}

void Ising_spacecorr::interval_and_file()
{
  obs_interval=par.value("ising_spacecorr.obs_interval").as<int>();
  obs_file_prefix=par.value("ising_spacecorr.obs_file_prefix").as<std::string>();
}

void Ising_spacecorr::write_header()
{
  fprintf(of,"# Ising space correlations\n# observaiton interval %d\n",obs_interval);
}

Ising_spacecorr::~Ising_spacecorr()
{
  if (of) {
    double magavsq=Mag.ave()/conf.lat->size();
    magavsq*=magavsq;
    double Ck0=Ck[0].ave()-Mag.ave()*Mag.ave()/conf.lat->size();
    double Ck1=Ck[1].ave();
    double xi2=sqrt(Ck0/Ck1-1)/(2*sin(deltak/2));

    fprintf(of,"# Mag = %g\n",Mag.ave());
    fprintf(of,"# MagSq = %g\n",MagSq.ave());
    fprintf(of,"# chi (per site) = %g\n",(MagSq.ave()-Mag.ave()*Mag.ave())/conf.lat->size());
    fprintf(of,"# xi2 = %g\n",xi2);
    fprintf(of,"# r  C_c(r)           k  C(k)\n");

    fprintf(of,"0 %g          0 %g\n",Cr0.ave()-magavsq,Ck0);
    bool moreCr= (*Cr)[0].N()>0;
    int i=0;
    while (moreCr || i+1<Ck.size() ) {
      if (moreCr)
	fprintf(of,"%g %g          ",Cr->binc(i),(*Cr)[i].ave()-magavsq);
      else fprintf(of," -- --                  ");
      if (i+1<Ck.size())
	fprintf(of,"%g %g",(i+1)*deltak,Ck[i+1].ave());
      fprintf(of,"\n");
      ++i;
      moreCr= i<Cr->nbins() && (*Cr)[i].N()>0;
    }
  }
}

// computation of C(k) and C(r) depends on lattice type

void Ising_spacecorr::observe()
{
  Mag.push(env.mag);
  MagSq.push(env.mag*env.mag);
  deltak=conf.Ck(Ck);
  conf.Cr(Cr0,Cr);
}  

#endif /* ISING_HH */

