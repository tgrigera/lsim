/* 
 * ising-space-corr.hh - Observable for the spatial spin-spin
 *                       correlation function
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#include "tsim/generic/geoave.hh"

template <typename configuration,typename environment>
class Ising_space_correlation : public Observable {
private:
  IsingSimulation<configuration,environment>    &sim;
  environment                                   &ienv;
  configuration                                 &iconf;
  std::string                                   correlation_fname;

  geoave                                        G,Gconn;

  void do_observation();

public:
  Ising_space_correlation(IsingSimulation<configuration,environment> &sim);
  ~Ising_space_correlation();
} ;

template <typename configuration,typename environment>
Ising_space_correlation<configuration,environment>::
Ising_space_correlation(IsingSimulation<configuration,environment> &s) :
  Observable("Ising space correlation",s.ienv,s.iconf,s.ienv.space_correlation_step),
  sim(s),
  iconf(s.iconf),
  ienv(s.ienv),
  correlation_fname(s.ienv.space_correlation_file),
  G(0,1,1),
  Gconn(0,1,1)
{
}

template <typename configuration,typename environment>
Ising_space_correlation<configuration,environment>::
~Ising_space_correlation()
{
  ofstream of(correlation_fname.c_str());

  of << "# Ising connected spin-spin correlations computed with time step = " << step << '\n';
  of << "# The connected G is computed substracting the instantaneous magnetization\n"
     << "# before doing the correlation (on a config-by-config basis)\n#\n";
  of << "# r  G(r)  G_conn(r)\n";

  std::vector<double> r,C,Cc,u,u1;
  G.get_aves(r,C,u,u1);
  Gconn.get_aves(r,Cc,u,u1);
  for (int i = 0; i<r.size(); ++i) {
    of <<  r[i] << ' ' << C[i] << ' ' << Cc[i] <<'\n';
  }
}

// template <typename environment>
// inline int sgcorrSC<environment>::pdiff(int i, int j, int L)
// {
//   return i-j-L*rint((double)(i-j)/L);
// }

template <>
void Ising_space_correlation<SQConf,IsingEnvironment>::do_observation()
{
  int Xhalf=iconf.lat.Xsize()/2;
  int Yhalf=iconf.lat.Ysize()/2;
  double m=(double) sim.mag/iconf.vol;
  for (int ix0=0; ix0<iconf.lat.Xsize(); ix0++)
    for (int iy0=0; iy0<iconf.lat.Ysize(); iy0++) {
      double s0=iconf.lat(ix0,iy0);
      for (int ix=ix0; ix<iconf.lat.Xsize(); ix++)
	for (int iy=iy0; iy<iconf.lat.Ysize(); iy++) {
	  int dx=iconf.lat.pdiffx(ix,ix0);
	  int dy=iconf.lat.pdiffy(iy,iy0);
	  double r=sqrt(dx*dx+dy*dy);
	  double s=iconf.lat(ix,iy);
	  G.push(r,s0*s);
	  Gconn.push(r,(s0-m)*(s-m));
	}
    }
}

