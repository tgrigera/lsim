/* 
 * isingSQ.hh - The Ising model on the square lattice
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2106, 2017
 * by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 * 
 */

#include <math.h>
#include <complex>

#include "glsim/exception.hh"
#include "glsim/lattice2D.hh"

/******************************************************************************
 *
 * Parameters and environment
 *
 */

#include "isingenv.hh"

class Ising_parameters : public glsim::Parameters {
public:
  Ising_parameters(const char *scope);
} ;

Ising_parameters::Ising_parameters(const char *scope) :
  Parameters(scope)
{
  parameter_file_options().add_options()
    ("ising.Lx",po::value<int>()->default_value(10),"")
    ("ising.Ly",po::value<int>()->default_value(10),"")
    ;
}

class Ising_environment : public Ising_environment_base {
public:
  int    Lx,Ly;
  Ising_environment(const char* scope=glsim::Parameters::default_scope);

protected:
  void  init_local(),warm_init_local();

private:
  Ising_parameters  par;

  void vserial(oarchive_t &ar) {ar << *this;}
  void vserial(iarchive_t &ar) {ar >> *this;}
  template <typename Archive>
  void serialize(Archive &ar,const unsigned int version);
  friend class boost::serialization::access;

public:
  static const int class_version=1;
} ;

BOOST_CLASS_VERSION(Ising_environment,Ising_environment::class_version);

Ising_environment::Ising_environment(const char* scope) :
  Ising_environment_base(scope),
  Lx(10), Ly(10),
  par(scope)
{}

void Ising_environment::init_local()
{
  Ising_environment_base::init_local();
  Lx=par.value("ising.Lx").as<int>();
  Ly=par.value("ising.Ly").as<int>();
}

void Ising_environment::warm_init_local()
{
  Ising_environment_base::warm_init_local();
}

template <typename Archive>
inline void Ising_environment::serialize(Archive &ar,const unsigned int version)
{
  if (version!=class_version)
    throw glsim::Environment_wrong_version("Ising_environment",version,class_version);
  ar & boost::serialization::base_object<Ising_environment_base>(*this);
  ar & Lx & Ly;
}


/******************************************************************************
 *
 * Configuration
 *
 */

#include "isingconf.hh"

typedef glsim::PeriodicSQLattice<short> SQLattice;

template <typename Graph>
void Ising_configuration<Graph>::create_lat(glsim::Environment *env)
{
  Ising_environment *ienv=dynamic_cast<Ising_environment*>(env);

  lat=new SQLattice(ienv->Lx,ienv->Ly);
}

template <typename Graph>
double Ising_configuration<Graph>::Ck(std::vector<glsim::AveVar<false>> &C)
{
  std::complex<double> I(0,1);
  double deltak=2*M_PI/lat->size_x();

  for (int ik=0; ik<C.size(); ++ik) {
    std::complex<double> Skx(0,0);
    std::complex<double> Sky(0,0);
    double kc=ik*deltak;
    for (auto iS1=lat->begin(); iS1!=lat->end(); ++iS1) {
      Skx+=exp( I * (kc*iS1.x()) ) * (double) (*iS1);
      Sky+=exp( I * (kc*iS1.y()) ) * (double) (*iS1);
    }
    C[ik].push(std::norm(Skx)/lat->size());
    C[ik].push(std::norm(Sky)/lat->size());
  }
  return deltak;
}

template <typename Graph>
void Ising_configuration<Graph>::Cr(glsim::AveVar<false> &Cr0,glsim::Binned_vector<glsim::AveVar<false>> *&Cr)
{
  if (Cr==0) {
    double rmax= lat->size_x()*lat->size_x() + lat->size_y()*lat->size_y();
    rmax/=4+1;  // safety margin
    Cr=new glsim::Binned_vector<glsim::AveVar<false>>(rmax/1.5,0,rmax);
  }

  for (auto iS1=lat->begin(); iS1!=lat->end(); ++iS1) {
    Cr0.push((*iS1)*(*iS1));
    auto iS2=iS1;
    for (++iS2; iS2!=lat->end(); ++iS2) {
      int dx=lat->pdiffx(iS1.x(),iS2.x());
      int dy=lat->pdiffy(iS1.y(),iS2.y());
      double r=sqrt(dx*dx+dy*dy);
      (*Cr)[r].push((*iS1)*(*iS2));
    }
  }
}

typedef Ising_configuration<SQLattice> Ising_conf;

#include "isingtraj.hh"

template<>
void Ising_trajectory<Ising_conf>::
store_positions(glsim::OLconfiguration *iconf,const Ising_conf &conf)
{
  int i=0;
  for (auto site=conf.lat->begin(); site!=conf.lat->end(); ++site) {
    iconf->r[i][0]=lattspace*site.x();
    iconf->r[i][1]=lattspace*site.y();
    iconf->r[i][2]=0;
    ++i;
  }
  iconf->box_length[0]=conf.lat->size_x()*lattspace;
  iconf->box_length[1]=conf.lat->size_y()*lattspace;
  iconf->box_length[2]=0;
}

/******************************************************************************
 *
 * Sim and main
 *
 */

#include "ising.hh"

const char* Ising_simulation::name() const
{
  return "Metropolis Monte Carlo of the ferromagnetic Ising model on the square lattice";
}

void wmain(int argc, char *argv[])
{
  Ising_environment  env;
  Ising_conf         conf(&env);
  Ising_observable   obs(env,conf);
  glsim::SimulationCL CL("isingSC","(C) 2017 Tomas S. Grigera",env.scope());
  Ising_trajectory<Ising_conf> traj(env,conf);
  Ising_spacecorr    spacecorr(env,conf);

  CL.parse_command_line(argc,argv);
  glsim::prepare(CL,env,conf);

  Ising_simulation_regular sim(env,conf);
  std::cout << "\n" << sim.name() << "\n\n";
  std::cout << "Lattice size = " << conf.lat->size_x() << "x" << conf.lat->size_y() << "\n\n";
 
 traj.observe_first();  // This is mandatory!
  sim.run();
  env.save();
  conf.save(env.configuration_file_fin);
}

int main(int argc, char *argv[])
{
  return glsim::StandardEC(argc,argv,wmain);
}

