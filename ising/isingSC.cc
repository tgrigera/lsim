/* 
 * isingSC.cc - The Ising model on the simple cubic lattice
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2106, 2017
 * by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 * 
 */

#include "glsim/exception.hh"
#include "glsim/lattice3D.hh"


/******************************************************************************
 *
 * Parameters and environment
 *
 */

#include "isingenv.hh"

class Ising_parameters : public glsim::Parameters {
public:
  Ising_parameters(const char *scope);
} ;

Ising_parameters::Ising_parameters(const char *scope) :
  Parameters(scope)
{
  parameter_file_options().add_options()
    ("ising.Lx",po::value<int>()->default_value(10),"")
    ("ising.Ly",po::value<int>()->default_value(10),"")
    ("ising.Lz",po::value<int>()->default_value(10),"")
    ;
}

class Ising_environment : public Ising_environment_base {
public:
  int    Lx,Ly,Lz;
  Ising_environment(const char* scope=glsim::Parameters::default_scope);

protected:
  void  init_local(),warm_init_local();

private:
  Ising_parameters  par;

  void vserial(oarchive_t &ar) {ar << *this;}
  void vserial(iarchive_t &ar) {ar >> *this;}
  template <typename Archive>
  void serialize(Archive &ar,const unsigned int version);
  friend class boost::serialization::access;

public:
  static const int class_version=1;
} ;

BOOST_CLASS_VERSION(Ising_environment,Ising_environment::class_version);

Ising_environment::Ising_environment(const char* scope) :
  Ising_environment_base(scope),
  Lx(10), Ly(10), Lz(10),
  par(scope)
{}

void Ising_environment::init_local()
{
  Ising_environment_base::init_local();
  Lx=par.value("ising.Lx").as<int>();
  Ly=par.value("ising.Ly").as<int>();
  Lz=par.value("ising.Lz").as<int>();
}

void Ising_environment::warm_init_local()
{
  Ising_environment_base::warm_init_local();
}

template <typename Archive>
inline void Ising_environment::serialize(Archive &ar,const unsigned int version)
{
  if (version!=class_version)
    throw glsim::Environment_wrong_version("Ising_environment",version,class_version);
  ar & boost::serialization::base_object<Ising_environment_base>(*this);
  ar & Lx & Ly & Lz;
}


/******************************************************************************
 *
 * Configuration
 *
 */

#include "isingconf.hh"

typedef glsim::PeriodicSCLattice<short> SCLattice;

template <typename Graph>
void Ising_configuration<Graph>::create_lat(glsim::Environment *env)
{
  Ising_environment *ienv=dynamic_cast<Ising_environment*>(env);

  lat=new SCLattice(ienv->Lx,ienv->Ly,ienv->Lz);
}

template <typename Graph>
double Ising_configuration<Graph>::Ck(std::vector<glsim::AveVar<false>> &C)
{
  throw glsim::Unimplemented("Ck");
}

template <typename Graph>
void Ising_configuration<Graph>::Cr(glsim::AveVar<false> &Cr0,glsim::Binned_vector<glsim::AveVar<false>> *&Cr)
{
  throw glsim::Unimplemented("Cr");
}

typedef Ising_configuration<SCLattice> Ising_conf;

#include "isingtraj.hh"

template<>
void Ising_trajectory<Ising_conf>::
store_positions(glsim::OLconfiguration *iconf,const Ising_conf &conf)
{
  int i=0;
  for (auto site=conf.lat->begin(); site!=conf.lat->end(); ++site) {
    iconf->r[i][0]=lattspace*site.x();
    iconf->r[i][1]=lattspace*site.y();
    iconf->r[i][2]=lattspace*site.z();
    ++i;
  }
  iconf->box_length[0]=conf.lat->size_x()*lattspace;
  iconf->box_length[1]=conf.lat->size_y()*lattspace;
  iconf->box_length[2]=conf.lat->size_z()*lattspace;
}


/******************************************************************************
 *
 * Sim and main
 *
 */

#include "ising.hh"

const char* Ising_simulation::name() const
{
  return "Ferromagnetic Ising model on the simple cubic lattice";
}

void wmain(int argc, char *argv[])
{
  Ising_environment  env;
  Ising_conf         conf(&env);
  Ising_observable   obs(env,conf);
  glsim::SimulationCL CL("isingSC","(C) 2017 Tomas S. Grigera",env.scope());
  Ising_trajectory<Ising_conf> traj(env,conf);

  CL.parse_command_line(argc,argv);
  glsim::prepare(CL,env,conf);

  Ising_simulation_regular sim(env,conf);
  traj.observe_first();  // This is mandatory!
  sim.run();
  env.save();
  conf.save(env.configuration_file_fin);
}

int main(int argc, char *argv[])
{
  return glsim::StandardEC(argc,argv,wmain);
}

