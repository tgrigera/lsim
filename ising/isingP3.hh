/* 
 * isingP3.hh - The Ising model on the 3-d Poisson graph
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2106, 2017
 * by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#ifndef ISINGP3_HH
#define ISINGP3_HH



#endif /* ISINGP3_HH */
