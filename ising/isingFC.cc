/* 
 * isingFC.hh - The Ising model on the fully connected graph
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#include <math.h>

#include "glsim/exception.hh"
#include "glsim/fcgraph.hh"

// // #include "glsim/errdeb.h"
// // #include "mc_parameters.hh"
// // #include "mc_environment.hh"
// // #include "isingconf.hh"
// // #include "fully-connected.hh"
// // #include "ising.hh"

/* 
 * The fully connected graph
 *
 */



// template<typename node,typename environment>
// class IsingSimulation<IsingConfiguration<FullyConnectedGraph<node> >,
// 		      environment> : public Simulation {
// private:
//   typedef       IsingConfiguration<FullyConnectedGraph<node> > FCconfig;
//   environment   &ienv;
//   FCconfig      &iconf;
//   double        h,mbeta;

//   double        energy;
//   long          mag;

//   void          EnerMag();

// public:
//   IsingSimulation(environment&,FCconfig&);

//   const char *name();
//   void step();
//   void log_start_run() {log();}
//   void log();

//   friend class Ising_standard_observer<FCconfig,environment>;
// } ;

// template<typename node,typename environment>
// IsingSimulation<IsingConfiguration<FullyConnectedGraph<node> >,
// 		environment>::IsingSimulation(environment &e,FCconfig &c) :
//   Simulation(e,c), 
//   ienv(e),
//   iconf(c)
// {
//   h=ienv.betah/ienv.beta;
//   mbeta=-ienv.beta;
//   EnerMag();
// }

// template<typename node,typename environment>
// void IsingSimulation<IsingConfiguration<FullyConnectedGraph<node> >,
// 		     environment>::EnerMag()
// {
//   mag=std::accumulate(iconf.lat.begin(),iconf.lat.end(),0L);
//   energy= - (double) 0.5*mag*(mag-1)/(iconf.lat.nNodes()-1) - h*mag;
// }

// template<typename node,typename environment>
// void IsingSimulation<IsingConfiguration<FullyConnectedGraph<node> >,
// 		     environment>::log()
// {
//   static char buf[200];
//   sprintf(buf,"Step %5d mag %12.5e ener %12.5e\n",
// 	  ienv.steps,(double) mag/iconf.vol,(double) energy/iconf.vol);
//   std::cout << buf;
// }


/******************************************************************************
 *
 * Parameters and environment
 *
 */

#include "isingenv.hh"

class Ising_parameters : public glsim::Parameters {
public:
  Ising_parameters(const char *scope);
} ;

Ising_parameters::Ising_parameters(const char *scope) :
  Parameters(scope)
{
  parameter_file_options().add_options()
    ("ising.N",po::value<int>()->default_value(10),"")
    ("ising.betah",po::value<double>()->default_value(0.),"h/kT")
    ;
}

class Ising_environment : public Ising_environment_base {
public:
  int    N;
  double betah;
  Ising_environment(const char* scope=glsim::Parameters::default_scope);

protected:
  void  init_local(),warm_init_local();

private:
  Ising_parameters  par;

  void vserial(oarchive_t &ar) {ar << *this;}
  void vserial(iarchive_t &ar) {ar >> *this;}
  template <typename Archive>
  void serialize(Archive &ar,const unsigned int version);
  friend class boost::serialization::access;

public:
  static const int class_version=1;
} ;

BOOST_CLASS_VERSION(Ising_environment,Ising_environment::class_version);

Ising_environment::Ising_environment(const char* scope) :
  Ising_environment_base(scope),
  N(100), betah(0.),
  par(scope)
{}

void Ising_environment::init_local()
{
  Ising_environment_base::init_local();
  N=par.value("ising.N").as<int>();
  betah=par.value("ising.betah").as<double>();
}

void Ising_environment::warm_init_local()
{
  Ising_environment_base::warm_init_local();
}

template <typename Archive>
inline void Ising_environment::serialize(Archive &ar,const unsigned int version)
{
  if (version!=class_version)
    throw glsim::Environment_wrong_version("Ising_environment",version,class_version);
  ar & boost::serialization::base_object<Ising_environment_base>(*this);
  ar & N & betah;
}

/******************************************************************************
 *
 * Configuration
 *
 */

#include "isingconf.hh"

typedef glsim::FullyConnectedGraph<short> FCGraph;

template <typename Graph>
void Ising_configuration<Graph>::create_lat(glsim::Environment *env)
{
  Ising_environment *ienv=dynamic_cast<Ising_environment*>(env);

  lat=new FCGraph(ienv->N);
}

template <typename Graph>
double Ising_configuration<Graph>::Ck(std::vector<glsim::AveVar<false>> &C)
{
  double SS=0.;
  for (auto iS1=lat->begin(); iS1!=lat->end(); ++iS1)
    SS+= *iS1;
  C[0].push(SS*SS/lat->size());
  return 0;
}

template <typename Graph>
void Ising_configuration<Graph>::Cr(glsim::AveVar<false> &Cr0,glsim::Binned_vector<glsim::AveVar<false>> *&Cr)
{}

typedef Ising_configuration<FCGraph> Ising_conf;

/******************************************************************************
 *
 * Sim and main
 *
 */

#include "ising.hh"

const char* Ising_simulation::name() const
{
  return "Metropolis Monte Carlo of the ferromagnetic Ising model on the fully-connected graph";
}

class Ising_simulation_FC : public Ising_simulation {
public:
  Ising_simulation_FC(Ising_environment& env,Ising_conf& conf) :
    Ising_simulation(env,conf) {}
  void step();

private:
  glsim::Uniform_real uran;
} ;

/**
   The general routine used in the regular lattice case  is very inefficient for the fully
   connected graph, because in this case the sum of the neighbours is the
   sum of the whole graph (except the flip candidate), so the general
   [[step]] performs as $O(N)$. Also, in this graph, the coupling must
   scale as $1/N$ for the energy to remain extensive. The energy is thus
   \f[ E(\{S_i\}) = -\frac{J}{2(N-1)} \sum_{i\neq j}^N S_i S_j - h \sum_i
    S_i = -\frac{J}{2(N-1)} (M^2 - M) - h M, 
   \f]
   with \f$M=\sum_i S_i\f$. Flipping spin \f$S_i\f$ will change the magnetization
   by \f$\Delta M = -2 S_i\f$ (\f$S_i\f$ is the value \emph{before} the
    flip). Then
    \f[
      \Delta E = E(\{S_1,\ldots,-S_i,\ldots,S_N\}) -
        E(\{S_1,\ldots,S_i,\ldots,S_N\})\\ 
     = \frac{J}{N-1}(2M -2S_i -1)S_i + 2h S_i.
    \f]

*/

void Ising_simulation_FC::step()
{
  FCGraph *lat=conf.lat;
  static FCGraph::node_iterator site(*lat);

  for (int tries=0; tries<lat->size(); tries++) {
    site.to(lat->data()+ran(lat->size()));
    double DE = (double) (2*env.mag-2*(*site)-1)*(*site)/(lat->size()-1) +
      2*env.field*(*site);
    if (DE<0 || uran()<=exp(-env.beta*DE) ) {
      env.mag+=(-2*(*site));
      *site*=-1;
      env.energy= - (double) 0.5*env.mag*(env.mag-1)/(lat->size()-1) - env.field*env.mag;
    }
  }
  conf.step=env.steps_completed;
  env.run_completed = env.steps_in_run>=env.MCsteps;
}


//
// Esto es para leer la configuracion inicial
// ojo que no es el formato en que escribe Graph
//
// template <>
// void IsingConfiguration<FCGraph>::load(const char *fname)
// {
//   FILE *file1;
//   int *punt;
//   int  i,j,fileLx,fileLy,seed1,seed2,nn,size;
//   char cad1[20];
//   float m0;

// /*	Leo la matriz con los parametros importante.  
// 	Esta debe ser una subrutina que va al programa de Tomas*/ 

//   file1 = fopen("mat.ini", "r");

// /*		Leo los parametros 					*/
//   fscanf(file1,"%s", cad1);
//   fscanf(file1,"%f", &m0);
//   fscanf(file1,"%s", cad1);
//   fscanf(file1,"%d", &seed1);
//   fscanf(file1,"%s", cad1);
//   fscanf(file1,"%d", &seed2);
//   fscanf(file1,"%s", cad1);
//   fscanf(file1,"%d", &fileLx);
//   fscanf(file1,"%s", cad1);
//   fscanf(file1,"%d", &fileLy);

//   int NN=fileLx*fileLy;

//   if (NN!=lat.nNodes()) {
//     fprintf(stderr,"ERROR: No coinciden tamagnos: N %d fileLx %d fileLy %d\n",lat.nNodes(),fileLx,fileLy);
//     exit(2);
//   }

// // //		Leo matriz					
// // 		size = (*Ly)*(*Lx);
// // 		if(!(punt = malloc(size*sizeof(int)))) 
// // 		{printf("No hay memoria suficiente \n");
// // 		exit(1);
// // 		}
// // //		punt = malloc(size*sizeof(int));	/*Se crea el espacio en memoria*/
// // 		*point = punt; 				/*pasa la direccion del puntero al principal*/ 


//   for (i=0; i<lat.nNodes(); i++)
//     fscanf(file1,"%3d", &lat(i)); /*Leo desde file1 y lo mando a la memoria */

//   fclose(file1);

//   std::cout << "Leida configuracion con magnetizacion " << m0 << "\n";
// }

void wmain(int argc, char *argv[])
{
  Ising_environment  env;
  Ising_conf         conf(&env);
  Ising_observable   obs(env,conf);
  glsim::SimulationCL CL("isingSC","(C) 2017 Tomas S. Grigera",env.scope());
  //Ising_trajectory<Ising_conf> traj(env,conf);
  //Ising_spacecorr    spacecorr(env,conf);

  CL.parse_command_line(argc,argv);
  glsim::prepare(CL,env,conf);

  Ising_simulation_FC sim(env,conf);
  std::cout << "\n" << sim.name() << "\n\n";
  std::cout << "Graph size = " << conf.lat->size() << " nodes\n\n";
 
  //traj.observe_first();  // This is mandatory!
  sim.run();
  env.save();
  conf.save(env.configuration_file_fin);
}

int main(int argc, char *argv[])
{
  return glsim::StandardEC(argc,argv,wmain);
}
