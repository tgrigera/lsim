/* 
 * isingP3.cc - The Ising model on the 3-d Poisson graph
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2106, 2017
 * by Tomas S. Grigera.
 * 
 * Modification or redistribution of tsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#include "glsim/exception.hh"
#include "glsim/poisson.hh"


/******************************************************************************
 *
 * Parameters and environment
 *
 */

#include "isingenv.hh"

class Ising_parameters : public glsim::Parameters {
public:
  Ising_parameters(const char *scope);
} ;

Ising_parameters::Ising_parameters(const char *scope) :
  Parameters(scope)
{
  parameter_file_options().add_options()
    ("ising.N",po::value<int>()->default_value(10),"")
    ("ising.cutoff",po::value<double>()->default_value(1.),"")
    ("ising.boxl",po::value<double>()->default_value(1.),"")
    ;
}

class Ising_environment : public Ising_environment_base {
public:
  int    N;
  double cutoff,boxl;
  Ising_environment(const char* scope=glsim::Parameters::default_scope);

protected:
  void  init_local(),warm_init_local();

private:
  Ising_parameters  par;

  void vserial(oarchive_t &ar) {ar << *this;}
  void vserial(iarchive_t &ar) {ar >> *this;}
  template <typename Archive>
  void serialize(Archive &ar,const unsigned int version);
  friend class boost::serialization::access;

public:
  static const int class_version=1;
} ;

BOOST_CLASS_VERSION(Ising_environment,Ising_environment::class_version);

Ising_environment::Ising_environment(const char* scope) :
  Ising_environment_base(scope),
  N(10), cutoff(1.), boxl(1.),
  par(scope)
{}

void Ising_environment::init_local()
{
  Ising_environment_base::init_local();
  N=par.value("ising.N").as<int>();
  cutoff=par.value("ising.cutoff").as<double>();
  boxl=par.value("ising.boxl").as<double>();
}

void Ising_environment::warm_init_local()
{
  Ising_environment_base::warm_init_local();
}

template <typename Archive>
inline void Ising_environment::serialize(Archive &ar,const unsigned int version)
{
  if (version!=class_version)
    throw glsim::Environment_wrong_version("Ising_environment",version,class_version);
  ar & boost::serialization::base_object<Ising_environment_base>(*this);
  ar & N & cutoff & boxl;
}


/******************************************************************************
 *
 * Configuration
 *
 */

#include "isingconf.hh"

typedef glsim::MetricPoisson3D<short> PLatt;

template <typename Graph>
void Ising_configuration<Graph>::create_lat(glsim::Environment *env)
{
  Ising_environment *ienv=dynamic_cast<Ising_environment*>(env);

  double box[3];
  box[0]=box[1]=box[2]=ienv->boxl;
  lat=new PLatt(ienv->N,box,ienv->cutoff,true);
}

template <typename Graph>
double Ising_configuration<Graph>::Ck(std::vector<glsim::AveVar<false>> &C)
{
  throw glsim::Unimplemented("Ck");
}

template <typename Graph>
void Ising_configuration<Graph>::Cr(glsim::AveVar<false> &Cr0,glsim::Binned_vector<glsim::AveVar<false>> *&Cr)
{
  throw glsim::Unimplemented("Cr");
}

typedef Ising_configuration<PLatt> Ising_conf;

#include "isingtraj.hh"

template<>
void Ising_trajectory<Ising_conf>::
store_positions(glsim::OLconfiguration *iconf,const Ising_conf &conf)
{
  memcpy(iconf->r,conf.lat->oconf().r,3*conf.lat->size()*sizeof(double));
  memcpy(iconf->box_length,conf.lat->oconf().box_length,3*sizeof(double));
}


/******************************************************************************
 *
 * Sim and main
 *
 */

#include "ising.hh"

const char* Ising_simulation::name() const
{
  return "Ferromagnetic Ising model on the (metric) 3-D Poisson graph";
}

void count_neighbours(PLatt* lat)
{
  long zav=0,zmin=lat->size(),zmax=-1;
  for (int i=0; i<lat->size(); ++i) {
    int z=lat->neighbour_size(i);
    if (z<zmin) zmin=z;
    if (z>zmax) zmax=z;
    zav+=z;
  }
  std::cout << "Lattice conectivity (number of neighbours):\n"
	    << "     Min     = " << zmin << '\n'
	    << "     Average = " << (double)zav/lat->size() << '\n'
	    << "     Max     = " << zmax << "\n\n";
}

void wmain(int argc, char *argv[])
{
  Ising_environment  env;
  Ising_conf         conf(&env);
  Ising_observable   obs(env,conf);
  glsim::SimulationCL CL("isingSC","(C) 2017 Tomas S. Grigera",env.scope());
  Ising_trajectory<Ising_conf> traj(env,conf);

  CL.parse_command_line(argc,argv);
  glsim::prepare(CL,env,conf);

  Ising_simulation_random sim(env,conf);
  count_neighbours(conf.lat);
  traj.observe_first();  // This is mandatory!
  sim.run();
  env.save();
  conf.save(env.configuration_file_fin);
}

int main(int argc, char *argv[])
{
  return glsim::StandardEC(argc,argv,wmain);
}

