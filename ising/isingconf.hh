/* 
 * isingconf.hh -- Ising configuration: incorporating the graph within
 *                 glsim configuration class
 *
 * This file is part of lsim
 * 
 * Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
 * 2009, 2010, 2011, 2012 by Tomas S. Grigera.
 * 
 * Modification or redistribution of lsim is NOT allowed. See the file
 * COPYING in the base directory.
 *
 */

#include <iostream>
#include "glsim/stochastic.hh"
#include "glsim/random.hh"
#include "glsim/configuration.hh"
#include "glsim/binvec.hh"
#include "glsim/avevar.hh"

#include "isingenv.hh"

class Lattice_create_pars {
} ;

template <typename Graph>
class Ising_configuration : public glsim::Configuration {
public:
  typedef Graph GraphT;
  GraphT  *lat;
  int     vol;
  long    step;

  Ising_configuration(Ising_environment_base* env_) : env(env_), lat(0) {}
  void init(const char*);
  void load(const char*);
  void save(const char*);
  double Ck(std::vector<glsim::AveVar<false>> &C);
  void Cr(glsim::AveVar<false> &Cr0,glsim::Binned_vector<glsim::AveVar<false>> *&Cr);

  using glsim::Configuration::save;
  using glsim::Configuration::load;

private:
  Ising_environment_base *env;
  void create_lat(glsim::Environment*);
} ;

template <typename Graph>
void Ising_configuration<Graph>::init(const char* c)
{
  if (c) {
    this->load(c);
    step=0;
    return;
  }

  if (lat) delete lat;
  create_lat(env);
  glsim::Uniform_integer ran;

  unsigned long half=(ran.min()+ran.max())/2;
  if (env->default_config_ordered) {
    for (auto s=lat->begin(); s!=lat->end(); s++)
      *s=1;
  } else {
    for (auto s=lat->begin(); s!=lat->end(); s++)
      *s = (ran.raw()>half) ? 1 : -1;
  }
  vol=lat->size();
  step=0;
}

template <typename Graph>
void Ising_configuration<Graph>::save(const char* file)
{
  std::ofstream f(file,std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);
  f.write((char*)&step,sizeof(step));
  lat->write(f);
}

template <typename Graph>
void Ising_configuration<Graph>::load(const char* file)
{
  if (!lat) create_lat(env);
  std::ifstream f(file,std::ios_base::in | std::ios_base::binary);
  if (!f) throw glsim::Runtime_error(std::string("open failed:")+strerror(errno));
  f.read((char*) &step,sizeof(step));
  lat->read(f);
  vol=lat->size();
}

